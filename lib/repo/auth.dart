import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_sign_in/google_sign_in.dart';

class User {
  final String uid;
  final String displayName;
  final String avatarUrl;
  final String email;

  const User(
      {@required this.uid, this.displayName, this.avatarUrl, this.email});
}

abstract class IAuth {
  Future<User> signInWithGoogle();

  Future<User> currentUser();

  Future<void> signOut();

  Stream<User> get onAuthStateChanged;

  String uid();
}

class AuthImplementation extends IAuth {
  final FirebaseAuth firebaseAuth = FirebaseAuth.instance;
  String mUid;

  User userFromFireBase(FirebaseUser user) {
    if (user == null) {
      return null;
    }
    mUid = user.email.isNotEmpty ? user.email : user.uid;
    return User(
        uid: user.email.isNotEmpty ? user.email : user.uid,
        displayName: user.displayName,
        avatarUrl: user.photoUrl,
        email: user.email);
  }

  @override
  Future<User> signInWithGoogle() async {
    final GoogleSignIn googleSignIn = GoogleSignIn();
    final GoogleSignInAccount googleSignInAccount = await googleSignIn.signIn();
    if (googleSignInAccount != null) {
      final GoogleSignInAuthentication googleAuth =
          await googleSignInAccount.authentication;
      if (googleAuth.idToken != null && googleAuth.accessToken != null) {
        final authResult = await firebaseAuth.signInWithCredential(
            GoogleAuthProvider.getCredential(
                idToken: googleAuth.idToken,
                accessToken: googleAuth.accessToken));

        return userFromFireBase(authResult.user);
      } else {
        throw PlatformException(
            code: "ERROR_MISSING_GOOGLE_AUTH_TOKEN",
            message: "ERROR_MISSING_GOOGLE_AUTH_TOKEN");
      }
    } else {
      throw PlatformException(
          code: "ERROR_ABORTED_BY_USER", message: "Sign in aborted by user");
    }
  }

  @override
  Future<User> currentUser() async {
    final user = await firebaseAuth.currentUser();
    if (user != null) {
      mUid = user.uid;
      return userFromFireBase(user);
    } else {
      return null;
    }

  }

  @override
  Future<void> signOut() async {
    final googleSignIn = GoogleSignIn();
    await googleSignIn.signOut();
    await firebaseAuth.signOut();
  }

  @override
  Stream<User> get onAuthStateChanged {
    return firebaseAuth.onAuthStateChanged
        .map((event) => userFromFireBase(event));
  }

  @override
  String uid() {
    return mUid;
  }
}
