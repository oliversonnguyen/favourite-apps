import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:my_favorite_apps/config/IPlatform.dart';

import 'package:my_favorite_apps/database/database.dart';
import 'package:my_favorite_apps/global_data/AppDataLocal.dart';
import 'package:my_favorite_apps/global_data/DiskCachedData.dart';

import 'package:my_favorite_apps/global_data/HiveAppItem.dart';
import 'package:my_favorite_apps/global_data/HiveAppItems.dart';
import 'package:my_favorite_apps/global_data/SourceData.dart';

import 'package:my_favorite_apps/global_data/app_data.dart';
import 'package:my_favorite_apps/repo/repos.dart';
import 'package:my_favorite_apps/screens/screens.dart';
import 'package:my_favorite_apps/config/dracula_theme.dart';
import 'package:provider/provider.dart';

void main() async {
  await Hive.initFlutter();

  Hive.registerAdapter(HiveAppItemAdapter());
  Hive.registerAdapter(HiveAppItemsAdapter());

  runApp(MyApp());
//  runApp(SplashScreen());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    var providerHome2 = MultiProvider(
      providers: [
        Provider<IPlatform>(
          create: (_) => PlatformImpl(),
        ),
        Provider<IAuth>(
          create: (_) => AuthImplementation(),
        ),
        Provider<AppDataLocal>(
          create: (_) => AppDataLocalImpl(),
        ),
        Provider<DiskCachedData>(
          create: (_) => DiskCachedDataImpl(),
        ),
      ],
      builder: (context, _) {
        return Provider<SourceData>(
          create: (_) => SourceDataImpl(context.read<IPlatform>()),
          builder: (context, _) {
            return Provider<AppData>(
              create: (_) => AllAppData(context.read<SourceData>(),
                  context.read<AppDataLocal>(), context.read<DiskCachedData>()),
              builder: (context, _) {
                return Provider<Database>(
                  create: (_) => FireBaseStoreDatabase(
                      auth: context.read<IAuth>(),
                      uid: context.read<IAuth>().uid()),
                  builder: (context, _) => NewHome.create(context),
                );
              },
            );
          },
        );
      },
    );

    return providerHome2;
  }
}

class NewHome extends StatefulWidget {
  @override
  _NewHomeState createState() => _NewHomeState();

  static Widget create(BuildContext context) {
    return NewHome();
  }
}

class _NewHomeState extends State<NewHome> {
  @override
  Widget build(BuildContext context) {
    final AppData app = context.watch();
    final IPlatform platform = context.watch();
    final IAuth auth = context.watch();

    var materialApp = MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Favorite Apps',
      theme: themeDracula,
      home: NavScreen(
        app: app,
        platform: platform,
        auth: auth,
      ),
    );

    return materialApp;
  }
}
