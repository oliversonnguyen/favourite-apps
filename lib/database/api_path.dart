class APIPath {
  // static String favoriteApps(String uid)  => "/users/$uid/favorite";
  // static String favoriteApps(String uid)  => "/path";
  // static String favoriteApps(String uid)  => "/users/";
  static String favoriteApps(String uid)  => "/fav";
  static String FAVORITE_COLLECTION(String uid)  => "/users/$uid/favorite";
  static String TAG(String uid)  => "/users/$uid/tags";
  static String APPTAG(String uid, String tag)  => "/users/$uid/tags/$tag/apps";
  static String PATH_APPS(String uid)  => "/users/$uid/apps";
  static String PATH_APPS_TAG(String uid, String package)  => "/users/$uid/apps/$package/tags";
  static String PATH_APPS_GLOBAL()  => "/apps";
  static String PATH_APPS_GLOBAL_COMMENT(String package)  => "/apps/$package/comments";
  static String PATH_SHARE()  => "/shares";

}