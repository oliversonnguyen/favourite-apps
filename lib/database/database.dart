import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:my_favorite_apps/app_model/tags/Tag.dart';
import 'package:my_favorite_apps/data/AppReview.dart';
import 'package:my_favorite_apps/data/AppShare.dart';
import 'package:my_favorite_apps/data/app_item.dart';
import 'package:my_favorite_apps/database/api_path.dart';
import 'package:my_favorite_apps/repo/auth.dart';

abstract class Database {
  Future<void> favoriteApp(AppItem app);

  Future<void> removeFavorite(AppItem app);

  Stream<List<AppItem>> favoriteStream();

  Stream<List<Tag>> userTags(String uid);

  Future<int> getHighestTagRank(String uid);

  Future<void> upRank(Tag tag);

  Future<void> addTag(Tag tag);

  Future<void> addAppTag(Tag tag, AppItem item);

  Future<void> addTagToApp(AppItem item, Tag tag);

  //return appID if add sucessfull
  Future<String> addApps(AppItem item);

  void redFavorite();

  Stream<List<Tag>> getAppTags(AppItem app, String uid);

  Future<void> removeAppTag(AppItem app, Tag tag);

  Stream<bool> isAppFavorite(AppItem app, String uid);

  Stream<List<Tag>> getUserTags();

  Stream<List<AppItem>> getTagApps(Tag tag);

  Future<void> addAppGlobal(AppItem item);

  Future<void> addCommentToApp(AppItem item, String comment);

  Stream<List<AppReview>> getAppReviews(AppItem item);

  Future<void> shareApp(AppItem item, String shareDesc);

  Stream<List<AppShare>> getAppsShare();
}

class FireBaseStoreDatabase extends Database {
  final String uid;

  final instanceFireStore = Firestore.instance;

  final IAuth auth;

  FireBaseStoreDatabase({this.auth, this.uid});

  @override
  Future<void> favoriteApp(AppItem app) async {
    final user = await auth.currentUser();
    final uid = user.uid;
    final path = APIPath.FAVORITE_COLLECTION(uid);

    print(">>>favoriteApp with path: $path and item: ${app.toString()}");

    final querySnapshot = await instanceFireStore
        .collection(path)
        .where('package', isEqualTo: app.packageName)
        .getDocuments();

    if (querySnapshot.documents.isEmpty) {
      print(">>>> package can add now");
      final Map<String, dynamic> data = {
        'appName': app.name,
        'desc': app.desc,
        'package': app.packageName
      };

      instanceFireStore
          .collection(path)
          .document(app.packageName)
          .setData(data)
          .then((value) => print(">>>favorite success ${app.name}"))
          .catchError((onError) => print("favorite error $onError"));
    } else {
      print(">>>> package already added");
    }
  }

  @override
  Stream<List<AppItem>> favoriteStream() {
    return auth.currentUser().asStream().asyncExpand((user) {
      final path = APIPath.FAVORITE_COLLECTION(user.uid);
      return instanceFireStore
          .collection(path)
          .snapshots()
          .map((event) => event.documents)
          .map((event) {
        List<AppItem> items = new List();
        event.forEach((docsSnapshot) {
          items.add(AppItem(
              name: docsSnapshot.data["appName"],
              desc: docsSnapshot.data["desc"],
              packageName: docsSnapshot.data["package"]));
        });
        return items;
      });
    });
  }

  @override
  Stream<List<Tag>> userTags(String uid) {
    final path = APIPath.TAG(uid);

    print(">>>userTags userTags $path");

    Query query = instanceFireStore.collection(path);
    return query.snapshots().map((querySnapshot) {
      return querySnapshot.documents;
    }).map((listDocsSnapshot) {
      List<Tag> items = new List();
      listDocsSnapshot.forEach((docsSnapshot) {
        items.add(Tag(
          name: docsSnapshot.data["name"],
        ));
      });
      return items;
    });
  }

  @override
  void redFavorite() {
    final path = APIPath.favoriteApps(uid);
    print("read favorite:$path");
    final reference = Firestore.instance.collection(path);
    final snapshots = reference.snapshots();
    snapshots.listen((snapshot) {
      snapshot.documents.forEach((element) {
        print("data:${element.data}");
      });
    });
  }

  @override
  Future<int> getHighestTagRank(String uid) async {
    final path = APIPath.TAG(uid);
    final querySnapshot = await instanceFireStore
        .collection(path)
        .orderBy("rank", descending: true)
        .limit(1)
        .getDocuments();
    if (querySnapshot.documents.isEmpty) {
      print(">>>getHighestTagRank empty");
      return 0;
    } else {
      print(">>>getHighestTagRank NOT empty");
      return querySnapshot.documents[0].data['rank'];
    }
  }

  @override
  Future<void> upRank(Tag tag) async {
    final user = await auth.currentUser();
    final uid = user.uid;
    final path = APIPath.TAG(uid);
    //find upper rank
    final querySnapshot = await instanceFireStore
        .collection(path)
        .where('rank', isEqualTo: tag.rank - 1)
        .limit(1)
        .getDocuments();

    var upRankName = querySnapshot.documents[0].data['name'];
    var upRankRank = querySnapshot.documents[0].data['rank'];

    final Map<String, dynamic> dataDown = {

      'rank': upRankRank + 1,
    };

    final Map<String, dynamic> dataUp = {

      'rank': tag.rank - 1,
    };
    instanceFireStore
        .collection(path)
        .document(upRankName)
        .updateData(dataDown)
    .catchError((Object error) {
      print(">>> update Object rank error $error");
    });


    instanceFireStore
        .collection(path)
        .document(tag.name)
        .updateData(dataUp);
  }

  @override
  Future<void> addTag(Tag item) async {
    final user = await auth.currentUser();
    final uid = user.uid;
    final path = APIPath.TAG(uid);
    print(">>>addTAg:$path and tag: ${item.toString()}");

    final querySnapshot = await instanceFireStore
        .collection(path)
        .where('name', isEqualTo: item.name.toUpperCase())
        .getDocuments();

    if (querySnapshot.documents.isEmpty) {
      print(">>>> tag can add now");
      var rank = await getHighestTagRank(user.uid) + 1;
      final Map<String, dynamic> data = {
        'name': item.name.toUpperCase(),
        'rank': rank,
      };
      instanceFireStore
          .collection(path)
          .document(item.name.toUpperCase())
          .setData(data)

          //.add(data)

          .then((value) {
        print(">>>>tag added:  and info: ${item.toString()}");
      }, onError: (Exception e) {
        print(">>>add tag ERROR $e");
      });
    } else {
      print(">>>> tag already added");
    }
  }

  @override
  Future<void> addAppTag(Tag tag, AppItem app) async {
    final user = await auth.currentUser();
    final uid = user.uid;
    final path = APIPath.APPTAG(uid, tag.name);
    print(">>>addAppTag:$path and tag: ${app.toString()}");

    final querySnapshot = await instanceFireStore
        .collection(path)
        .where('package', isEqualTo: app.packageName)
        .getDocuments();

    if (querySnapshot.documents.isEmpty) {
      print(">>>> tag can add now");
      final Map<String, dynamic> data = {
        'appName': app.name,
        'desc': app.desc,
        'package': app.packageName
      };

      instanceFireStore
          .collection(path)
          .document(app.packageName)
          .setData(data)
          .then((value) =>
              print("AddAppTag success app ${app.name} into tag ${tag.name}"))
          .catchError((onError) => print(">>>ADdAppTag error"));
    }
  }

  @override
  Future<void> addTagToApp(AppItem app, Tag tag) async {
    print(">>>addTagToApp processing");
    await addApps(app);

    final user = await auth.currentUser();
    final uid = user.uid;
    final path = APIPath.PATH_APPS_TAG(uid, app.packageName);
    print(">>>addTagToApp 2:$path ");

    final querySnapshot = await instanceFireStore
        .collection(path)
        .where('package', isEqualTo: app.packageName)
        .getDocuments();

    if (querySnapshot.documents.isEmpty) {
      print(">>>> addTagToApp can add now");
      final Map<String, dynamic> data = {
        'name': tag.name,
      };
      instanceFireStore.collection(path).document(tag.name).setData(data).then(
          (value) {
        print(">>>>addTagToApp added:and info: ${app.toString()}");
      }, onError: (Exception e) {
        print(">>>addTagToApp tag ERROR $e");
      });
    } else {
      print(">>>> addTagToApp already added");
    }
  }

  @override
  Future<String> addApps(AppItem app) async {
    final user = await auth.currentUser();
    final uid = user.uid;
    final path = APIPath.PATH_APPS(uid);
    print(">>>addApps:$path ");

    final querySnapshot = await instanceFireStore
        .collection(path)
        .where('package', isEqualTo: app.packageName)
        .getDocuments();

    if (querySnapshot.documents.isEmpty) {
      print(">>>> addApps can add now");
      final Map<String, dynamic> data = {
        'appName': app.name,
        'desc': app.desc,
        'package': app.packageName
      };
      await instanceFireStore
          .collection(path)
          .document(app.packageName)
          .setData(data)
          .whenComplete(() {
        print(">>>AddApp complete with id: ${app.packageName}");
      });
    } else {
      print(">>>> addApps already added");
      return null;
    }
  }

  @override
  Stream<List<Tag>> getAppTags(AppItem app, String uid) {
    final path = APIPath.PATH_APPS_TAG(uid, app.packageName);

    print(">>>getAppTags  $path");

    Query query = instanceFireStore.collection(path);
    return query.snapshots().map((querySnapshot) {
      return querySnapshot.documents;
    }).map((listDocsSnapshot) {
      List<Tag> items = new List();
      listDocsSnapshot.forEach((docsSnapshot) {
        items.add(Tag(
          name: docsSnapshot.data["name"],
        ));
      });
      return items;
    });
  }

  @override
  Future<void> removeAppTag(AppItem app, Tag tag) async {
    final user = await auth.currentUser();
    final uid = user.uid;
    final path = APIPath.PATH_APPS_TAG(uid, app.packageName);
    instanceFireStore
        .collection(path)
        .document(tag.name)
        .delete()
        .then((value) => print(">>>removeAppTag tag ${app.name} - ${tag.name}"))
        .catchError((onError) => print(">>>removeAppTag failed"));
  }

  @override
  Future<void> removeFavorite(AppItem app) async {
    final user = await auth.currentUser();
    final uid = user.uid;
    final path = APIPath.FAVORITE_COLLECTION(uid);
    print(">>>removeFavorite ${app.name} path: $path");
    instanceFireStore
        .collection(path)
        .document(app.packageName)
        .delete()
        .then((value) => print(">>>removeFavorite app ${app.name} "))
        .catchError((onError) => print(">>>removeFavorite failed"));
  }

  @override
  Stream<bool> isAppFavorite(AppItem app, String uid) {
    return auth.currentUser().asStream().asyncExpand((user) {
      final path = APIPath.FAVORITE_COLLECTION(user.uid);
      print(">>>isAppFavorite2 ${app.name} - path: $path");
      return instanceFireStore
          .collection(path)
          .where("package", isEqualTo: app.packageName)
          .snapshots()
          .map((querySnapshot) {
        return querySnapshot.documents;
      }).map((listDocsSnapshot) {
        List<Tag> items = new List();
//        listDocsSnapshot.forEach((docsSnapshot) {
//          print(">>>isAppFavorite6 ${docsSnapshot.data} ");
//        });
        print(">>>isAppFavorite5 ${listDocsSnapshot.length} ");
        return listDocsSnapshot.isNotEmpty;
      });
    });
  }

  @override
  Stream<List<Tag>> getUserTags() {
    print(">>>getUserTags");
    return auth.currentUser().asStream().asyncExpand((user) {
      final path = APIPath.TAG(user.uid);
      print(">>>getUserTags path: $path");
      return instanceFireStore
          .collection(path)
          .orderBy("rank", descending: false)
          .snapshots()
          .map((event) => event.documents)
          .map((event) {
        List<Tag> items = new List();
        event.forEach((element) {
          items
              .add(Tag(name: element.data["name"], rank: element.data["rank"]));
        });
        print(">>>getUserTags with item : ${items.length}");
        return items;
      });
    });
  }

  @override
  Stream<List<AppItem>> getTagApps(Tag tag) {
    print(">>>getTagApps");
    return auth.currentUser().asStream().asyncExpand((user) {
      final path = APIPath.APPTAG(user.uid, tag.name);
      return instanceFireStore
          .collection(path)
          .snapshots()
          .map((event) => event.documents)
          .map((event) {
        List<AppItem> items = new List();
        event.forEach((element) {
          items.add(AppItem(
            name: element.data["appName"],
            packageName: element.data["package"],
            desc: element.data["desc"],
          ));
        });
        return items;
      });
    });
  }

  @override
  Future<void> addAppGlobal(AppItem item) async {
    final path = APIPath.PATH_APPS_GLOBAL();

    final Map<String, dynamic> data = {
      'appName': item.name,
      'desc': item.desc,
      'package': item.packageName
    };
    instanceFireStore
        .collection(path)
        .document(item.packageName)
        .setData(data)
        .then((value) => print(">>>AddAppGlobal ${item.name}"))
        .catchError((onError) => print("ERROR $onError"));
  }

  @override
  Future<void> addCommentToApp(AppItem item, String comment) async {
    await addAppGlobal(item);
    final user = await auth.currentUser();
    final path = APIPath.PATH_APPS_GLOBAL_COMMENT(item.packageName);

    final Map<String, dynamic> data = {
      'author': user.displayName,
      "uid": user.email,
      "authorAvatar": user.avatarUrl,
      'comment': comment,
      "createAt": DateTime.now(),
    };

    instanceFireStore
        .collection(path)
        .document(user.email + "_" + DateTime.now().toString())
        .setData(data)
        .then((value) => print(">>>addCommentToApp ${item.name}"))
        .catchError((onError) => print("ERROR $onError"));
  }

  @override
  Stream<List<AppReview>> getAppReviews(AppItem item) {
    final path = APIPath.PATH_APPS_GLOBAL_COMMENT(item.packageName);
    print(">>>getAppReviews item:${item.name} with path: $path");
    return instanceFireStore
        .collection(path)
        .orderBy("createAt", descending: true)
        .snapshots()
        .map((event) => event.documents)
        .map((event) {
      print(">>>getAppReviews item:${event.length} with path: $path");
      List<AppReview> items = new List();
      event.forEach((element) {
        print(">>>getAppReviews item 1 :${element.data} with path: $path");
        items.add(AppReview(
            author: element.data["author"],
            authorAvatar: element.data["authorAvatar"],
            authorUid: element.data["uid"],
            comment: element.data["comment"],
            createAt: element.data["createAt"],
            item: item));
      });
      print(">>>getAppReviews item 2:${items.length} with path: $path");
      return items;
    });
  }

  @override
  Future<void> shareApp(AppItem item, String shareDesc) async {
    final user = await auth.currentUser();
    final path = APIPath.PATH_SHARE();
    final Map<String, dynamic> data = {
      'author': user.displayName,
      "uid": user.email,
      "authorAvatar": user.avatarUrl,
      'shareDesc': shareDesc,
      "createAt": DateTime.now(),
      "appName": item.name,
      "package": item.packageName,
      "appDesc": item.desc
    };

    instanceFireStore
        .collection(path)
        .document(
            item.name + "_" + user.email + "_" + DateTime.now().toString())
        .setData(data)
        .then((value) => print(">>>shareApp ${item.name}"))
        .catchError((onError) => print("shareApp ERROR $onError"));
  }

  @override
  Stream<List<AppShare>> getAppsShare() {
    final path = APIPath.PATH_SHARE();
    return instanceFireStore
        .collection(path)
        .orderBy("createAt", descending: true)
        .snapshots()
        .map((event) => event.documents)
        .map((event) {
      print(">>>getAppsShare item:${event.length} with path: $path");
      List<AppShare> items = new List();
      event.forEach((element) {
        print(">>>getAppsShare item 1 :${element.data} with path: $path");
        items.add(AppShare(
          author: element.data["author"],
          authorAvatar: element.data["authorAvatar"],
          uid: element.data["uid"],
          shareDesc: element.data["shareDesc"],
          appName: element.data["appName"],
          package: element.data["package"],
          appDesc: element.data["appDesc"],
          createAt: element.data["createAt"],
        ));
      });
      print(">>>getAppsShare item :${items.length} with path: $path");
      return items;
    });
  }
}
