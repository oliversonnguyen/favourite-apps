import 'package:flutter/material.dart';
import 'package:my_favorite_apps/app_model/tags/Tag.dart';
import 'package:my_favorite_apps/repo/auth.dart';
import 'package:my_favorite_apps/screens/all_group/components/ItemAllGroup.dart';
import 'package:my_favorite_apps/screens/all_group/components/SectionTitle.dart';
import 'package:my_favorite_apps/screens/all_group/components/UserGroupCard.dart';
import 'package:my_favorite_apps/screens/groups/components/TitleText.dart';
import 'package:my_favorite_apps/screens/login/LoginScreen.dart';
import 'package:my_favorite_apps/widgets/LoginWithDescWidget.dart';
import 'package:my_favorite_apps/widgets/loading.dart';

import '../BlocAllGroup.dart';

class AllGroupBody extends StatelessWidget {
  final BlocAllGroup bloc;

  AllGroupBody({this.bloc});

  @override
  Widget build(BuildContext context) {
    var bodyWidget = StreamBuilder<User>(
      stream: bloc.auth.onAuthStateChanged,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return SafeArea(
              child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(
                  height: 8,
                ),
                ItemUserGroup(
                  bloc: bloc,
                ),
              ],
            ),
          ));
        } else {
          return Loading();

//          return LoginWithDescWidget(
//            desc: "Tag your favorite apps",
//            auth: bloc.auth,
//          );
        }
      },
    );

    var o1 = SafeArea(
        child: SingleChildScrollView(
      child: Column(
        children: [
          SizedBox(
            height: 8,
          ),
          ItemUserGroup(
            bloc: bloc,
          ),
        ],
      ),
    ));
    return bodyWidget;
  }
}

class ItemUserGroup extends StatelessWidget {
  final BlocAllGroup bloc;

  ItemUserGroup({this.bloc});

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: bloc.getUserTags(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          final items = snapshot.data;
          return Column(
            children: List.generate(items.length, (index) {
              return Column(
                children: [
                  ItemAllGroup(
                    tag: items[index],
                    bloc: bloc,
                  ),
                  SizedBox(
                    height: 16,
                  ),
                ],
              );
            }),
          );
        } else {
          return TitleText(
            title: "You don't have any tag yet!",
          );
        }
      },
    );
  }
}
