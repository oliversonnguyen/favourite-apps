import 'package:flutter/material.dart';
import 'package:my_favorite_apps/config/palette.dart';
import 'package:my_favorite_apps/screens/groups/components/TitleText.dart';

class SectionTitle extends StatelessWidget {
  final String title;
  final Function onTap;

  const SectionTitle({Key key, this.title, this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          TitleText(
            title: title,
          ),
          FlatButton(
              onPressed: onTap,
              child: Icon(Icons.arrow_upward, color: Colors.green)),
//          FlatButton.icon(
//            onPressed: () => {},
//            icon: Icon(Icons.arrow_upward, color: Colors.green),
//            label: Text(''),
//          ),
//          FlatButton(
//            child: Text("See More", style: TextStyle(color: Palette.subTitleColor),),
//            onPressed: onTap,
//          ),
        ],
      ),
    );
  }
}
