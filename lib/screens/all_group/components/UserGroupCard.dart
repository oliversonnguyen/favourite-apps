import 'package:flutter/material.dart';
import 'package:my_favorite_apps/data/app_item.dart';
import 'package:my_favorite_apps/screens/app_detail/app_detail.dart';
import 'package:my_favorite_apps/screens/groups/components/TitleText.dart';

class UserGroupCard extends StatelessWidget {
  final AppItem item;
  final String name;

  const UserGroupCard({Key key, this.name, this.item}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(20),
      child: FlatButton(
        onPressed: () {
           AppDetail.show(context, item);
        },
        child: Stack(
          children: [
            SizedBox(
              width: 120,
              height: 60,
              child: Container(
                color: Colors.blue,
              ),
            ),
            Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    Color(0xFF343434).withOpacity(0.4),
                    Color(0xFF343434).withOpacity(0.15),
                  ],
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                horizontal: 15,
                vertical: 10,
              ),
              child: TitleText(
                title: name,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
