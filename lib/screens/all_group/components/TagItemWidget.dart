import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:device_apps/device_apps.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:my_favorite_apps/config/IPlatform.dart';
import 'package:my_favorite_apps/config/palette.dart';
import 'package:my_favorite_apps/data/app_item.dart';
import 'package:my_favorite_apps/screens/app_detail/app_detail.dart';
import 'package:url_launcher/url_launcher.dart';

class TagItemWidget extends StatelessWidget {
  final AppItem item;
  final IPlatform platform;

  const TagItemWidget({Key key, this.item, this.platform}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      onPressed: () {
        FocusScope.of(context).unfocus();
        AppDetail.show(context, item);
      },
      child: Stack(
        children: <Widget>[
          ClipRRect(
            borderRadius: BorderRadius.circular(12.0),
            child: (item.myIcon != null)
                ? Image.memory(
                    item.myIcon,
                    width: 110.0,
                    height: 80.0,
                  )
                : Container(
                    width: 110,
                    height: 80,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        gradient: Palette.storyGradient,
                        borderRadius: BorderRadius.circular(12.0)),
                  ),
          ),
          Container(
//          height: double.infinity,
            height: 80,
            width: 110.0,
            decoration: BoxDecoration(
                gradient: Palette.storyGradient,
                borderRadius: BorderRadius.circular(12.0)),
          ),
          Positioned(
            top: 0.0,
            right: 4.0,
            child: IconButton(
              padding: EdgeInsets.zero,
              icon: const Icon(
                MdiIcons.openInApp,
                color: Colors.white,
              ),
              iconSize: 30.0,
              color: Palette.facebookBlue,
              onPressed: () async {
                platform.openApp(item);
              },
            ),
          ),
          Positioned(
            bottom: 8.0,
            left: 8.0,
            right: 8.0,
            child: Text(
              item.name,
              style:
                  TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
            ),
          )
        ],
      ),
    );
  }
}
