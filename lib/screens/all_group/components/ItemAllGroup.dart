import 'package:flutter/material.dart';
import 'package:my_favorite_apps/app_model/tags/Tag.dart';
import 'package:my_favorite_apps/data/app_item.dart';
import 'package:my_favorite_apps/screens/all_group/BlocAllGroup.dart';
import 'package:my_favorite_apps/screens/all_group/components/SectionTitle.dart';
import 'package:my_favorite_apps/screens/all_group/components/TagItemWidget.dart';
import 'package:my_favorite_apps/screens/groups/components/TitleText.dart';
import 'package:my_favorite_apps/widgets/loading.dart';

class ItemAllGroup extends StatelessWidget {
  final Tag tag;
  final BlocAllGroup bloc;

  const ItemAllGroup({Key key, this.tag, this.bloc}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(4),
          color: Colors.black26,
        ),
        child: Column(
          children: [
            SizedBox(
              height: 8,
            ),
            SectionTitle(
              title: tag.name,
              onTap: ()=> bloc.upRank(tag),
            ),
            SizedBox(
              height: 8,
            ),
            StreamBuilder<List<AppItem>>(
              stream: bloc.getTagApps(Tag(name: tag.name)),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  final items = snapshot.data;
                  return SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: List.generate(items.length, (index) {
                        return TagItemWidget(
                          item: items[index],
                          platform: bloc.platform,
                        );
                        //return UserGroupCard(name: items[index].name, item: items[index],);
                      }),
                    ),
                  );
                } else {
                  return TitleText(
                    title: "No data, please login first",
                  );
                }
              },
            ),
            SizedBox(
              height: 16,
            )
          ],
        ),
      ),
    );
  }
}
