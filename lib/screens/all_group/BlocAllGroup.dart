import 'package:my_favorite_apps/app_model/tags/Tag.dart';
import 'package:my_favorite_apps/config/IPlatform.dart';
import 'package:my_favorite_apps/data/app_item.dart';
import 'package:my_favorite_apps/database/database.dart';
import 'package:my_favorite_apps/global_data/app_data.dart';
import 'package:my_favorite_apps/repo/auth.dart';
import 'package:rxdart/rxdart.dart';

class BlocAllGroup {
  final IAuth auth;
  final Database database;
  final AppData appData;
  final IPlatform platform;

  BlocAllGroup({this.auth, this.database, this.appData, this.platform});

  Stream<List<Tag>> getUserTags() => database.getUserTags();

  Stream<List<AppItem>> getTagApps(Tag tag) {
    return Rx.combineLatest2(
        database.getTagApps(tag), appData.getAllAppsStream(),
            (List<AppItem> favList, List<AppItem> installedList) {
          List<AppItem> result = new List();
          favList.forEach((favItem) {
            var compairItem = installedList.firstWhere((installedItem) {
              return installedItem.packageName == favItem.packageName;
            }, orElse: () => null);
            if (compairItem != null) {
              result.add(compairItem);
            } else {
              result.add(favItem);
            }
          });

          return result;
        });

    //return database.getTagApps(tag);
  }

  void upRank(Tag tag) {
    database.upRank(tag);
  }

  Future<void> init() {}
}
