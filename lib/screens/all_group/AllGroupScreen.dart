import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:my_favorite_apps/config/IPlatform.dart';
import 'package:my_favorite_apps/database/database.dart';
import 'package:my_favorite_apps/global_data/app_data.dart';
import 'package:my_favorite_apps/repo/auth.dart';
import 'package:my_favorite_apps/screens/all_group/BlocAllGroup.dart';
import 'package:my_favorite_apps/screens/all_group/components/AllGroupBody.dart';
import 'package:my_favorite_apps/screens/groups/components/TitleText.dart';
import 'package:provider/provider.dart';

class AllGroupScreen extends StatefulWidget {
  final BlocAllGroup bloc;

  const AllGroupScreen({Key key, this.bloc}) : super(key: key);

  @override
  _AllGroupScreenState createState() => _AllGroupScreenState();

  static Widget create(BuildContext context) {
    final Database database = Provider.of(context);
    final IAuth auth = Provider.of(context);
    final AppData appData = Provider.of(context);
    final IPlatform platform = context.watch();
    return Provider<BlocAllGroup>(
      create: (_) =>
          BlocAllGroup(auth: auth, database: database, appData: appData, platform: platform),
      builder: (context, _) {
        return AllGroupScreen(
          bloc: context.watch(),
        );
      },
    );
  }
}

class _AllGroupScreenState extends State<AllGroupScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: TitleText(
          title: "User Group",
        ),
      ),
      body: AllGroupBody(
        bloc: widget.bloc,
      ),
    );
  }
}
