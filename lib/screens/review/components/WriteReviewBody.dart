import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:my_favorite_apps/data/app_item.dart';
import 'package:my_favorite_apps/screens/review/components/BlocWriteReview.dart';

class WriteReviewBody extends StatefulWidget {
  final AppItem item;
  final BlocWriteReview bloc;

  WriteReviewBody({this.item, this.bloc});

  @override
  _WriteReviewBodyState createState() => _WriteReviewBodyState();
}

class _WriteReviewBodyState extends State<WriteReviewBody> {
  TextEditingController controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        TextField(
          controller: controller,
          style: TextStyle(color: Colors.white),
          decoration: InputDecoration(
            hintText: "How do you like about this app?",
          ),
        ),
        FlatButton.icon(
          onPressed: () {
            var content = controller.text;
            print(">>>submit $content");
            widget.bloc.sendComments(content, widget.item);
            Navigator.pop(context);
          },
          icon: Icon(Icons.send),
          label: Text("Submit"),
        )
      ],
    );
  }
}
