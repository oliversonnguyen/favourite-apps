import 'package:flutter/material.dart';
import 'package:my_favorite_apps/data/AppReview.dart';
import 'package:my_favorite_apps/global_data/ShareHeaderData.dart';
import 'package:my_favorite_apps/screens/discover/components/ShareHeader.dart';

class ReviewSectionWidget extends StatelessWidget {
  final AppReview item;

  const ReviewSectionWidget({Key key, this.item}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var returnPage2 = Container(
      margin: const EdgeInsets.symmetric(vertical: 5.0),
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        color: Colors.black26,
      ),
      child: Column(children: <Widget>[
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 12.0),

          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              ShareHeader(
                data: ShareHeaderData(
                    uid: item.authorUid,
                    authorAvatar: item.authorAvatar,
                    author: item.author,
                    createAt: item.createAt),
              ),
              const SizedBox(
                height: 4.0,
              ),
              Container(
                padding: EdgeInsets.symmetric(vertical: 8),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(2),
                  color: Colors.black12,
                ),
                child: Text(
                  item.comment,
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ],
          ),
        ),
      ]),
    );
    return returnPage2;
  }
}
