import 'package:my_favorite_apps/data/app_item.dart';
import 'package:my_favorite_apps/database/database.dart';
import 'package:my_favorite_apps/repo/auth.dart';

class BlocWriteReview {
  final Database database;
  final IAuth auth;
  final AppItem item;

  BlocWriteReview({this.database, this.auth, this.item});

  Future<void> dispose() {}

  Future<void> sendComments(String content, AppItem  item) {
    return database.addCommentToApp(item, content );

  }
}
