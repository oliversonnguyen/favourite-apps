import 'package:flutter/material.dart';
import 'package:my_favorite_apps/data/app_item.dart';
import 'package:my_favorite_apps/database/database.dart';
import 'package:my_favorite_apps/repo/auth.dart';
import 'package:my_favorite_apps/screens/review/components/BlocWriteReview.dart';
import 'package:my_favorite_apps/screens/review/components/WriteReviewBody.dart';
import 'package:provider/provider.dart';

class WriteReviewScreen extends StatefulWidget {

  final AppItem item;
  final BlocWriteReview bloc;

  WriteReviewScreen({this.item, this.bloc});
  @override
  _WriteReviewScreenState createState() => _WriteReviewScreenState(item: item, bloc: bloc);

  static Future<void> navigate(BuildContext context, AppItem appItem) async {
    await Navigator.of(context, rootNavigator: true).push(MaterialPageRoute(
        builder: (context) => WriteReviewScreen.create(context, appItem),
        fullscreenDialog: true));
  }

  static Widget create(BuildContext context, AppItem item) {
    final Database database = context.watch();
    final IAuth auth = context.watch();
    return Provider<BlocWriteReview>(
      create: (_) => BlocWriteReview(database: database, auth: auth, item: item),
      dispose: (context, bloc) => bloc.dispose(),
      builder: (context, _) {
        return WriteReviewScreen(
          item: item,
          bloc: context.watch(),
        );
      },
    );
  }
}

class _WriteReviewScreenState extends State<WriteReviewScreen> {
  final AppItem item;
  final BlocWriteReview bloc;

  _WriteReviewScreenState({this.item, this.bloc});


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(item.name),),
      body: WriteReviewBody(bloc: bloc, item: item,),

    );
  }
}
