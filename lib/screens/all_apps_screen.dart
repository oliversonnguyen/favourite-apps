import 'package:device_apps/device_apps.dart';
import 'package:flutter/material.dart';
import 'package:my_favorite_apps/config/IPlatform.dart';
import 'package:my_favorite_apps/config/palette.dart';
import 'package:my_favorite_apps/data/datas.dart';
import 'package:my_favorite_apps/global_data/app_data.dart';
import 'package:my_favorite_apps/screens/app_detail/app_detail.dart';
import 'package:my_favorite_apps/widgets/loading.dart';

class ScreenInstalledApps extends StatefulWidget {
  final AppData app;
  final IPlatform platform;

  const ScreenInstalledApps({Key key, this.app, this.platform})
      : super(key: key);

  @override
  _ScreenInstalledAppsState createState() =>
      _ScreenInstalledAppsState(app, platform);
}

class _ScreenInstalledAppsState extends State<ScreenInstalledApps> {
  final AppData app;
  final IPlatform platform;

  _ScreenInstalledAppsState(this.app, this.platform);

  @override
  void initState() {
    print(">>>FIRST INIT");

    super.initState();
    app.init();
  }

  @override
  Widget build(BuildContext context) {
    var p2 = StreamBuilder<List<AppItem>>(
      stream: app.getAllAppsWithCacheStream(),
      builder: (context, snapshot) {
        Widget returnWidget;
        print(">>>Stream p2 ${snapshot.hasData}");

        if (snapshot.hasData) {
          returnWidget = AllAppsScreen(
            items: snapshot.data,
            platform: platform,
          );
        } else if (snapshot.hasError) {
          returnWidget = Padding(
            padding: const EdgeInsets.only(top: 16),
            child: Text('Error: ${snapshot.error}'),
          );
        } else {
          returnWidget = Loading();
        }

        return returnWidget;
      },
    );
    return p2;
  }
}

class AllAppsScreen extends StatelessWidget {
  final List<AppItem> items;
  final IPlatform platform;

  const AllAppsScreen({Key key, this.items, this.platform}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
        padding: EdgeInsets.symmetric(vertical: 24.0, horizontal: 16.0),
        separatorBuilder: (context, index) {
          return SizedBox(height: 8.0);
        },
        itemCount: items.length,
        itemBuilder: (BuildContext context, int index) {
          final AppItem item = items[index];
          return AllAppItem(
            item: item,
            platform: platform,
          );
        });
  }
}

class AllAppItem extends StatelessWidget {
  final AppItem item;
  final IPlatform platform;

  const AllAppItem({Key key, this.item, this.platform}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var row = Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        //Icon(Icons.gamepad),
        (item.myIcon != null)
            ? Image.memory(
                item.myIcon,
                width: 40.0,
                height: 40.0,
              )
            : SizedBox.shrink(),
        SizedBox(
          width: 8.0,
        ),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                item.name,
                style: TextStyle(
                    fontWeight: FontWeight.w600, color: Palette.titleColor),
              ),
              SizedBox(
                height: 4.0,
              ),
              Text(
                item.packageName,
                style: TextStyle(fontSize: 12.0, color: Palette.subTitleColor),
              ),
            ],
          ),
        ),
        SizedBox(
          width: 8.0,
        ),
        Column(
          children: [
            FlatButton(
              minWidth: 0,
              //materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
              height: 28.0,
              onPressed: () async {
                platform.openApp(item);
              },
              color: Colors.white12,

              padding: EdgeInsets.symmetric(vertical: 0.0, horizontal: 16.0),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(12.0)),
              child: Text(
                item.isInstalled ? "OPEN" : "GET",
                style: TextStyle(
                    color: Colors.blue,
                    fontWeight: FontWeight.bold,
                    fontSize: 14.0),
              ),
            ),
          ],
        ),
      ],
    );
    var itemRow = FlatButton(
        onPressed: () {
          AppDetail.show(context, item);
          FocusScope.of(context).unfocus();
        },
        child: row);

    return itemRow;
  }
}
