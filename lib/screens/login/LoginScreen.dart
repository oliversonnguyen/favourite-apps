import 'package:flutter/material.dart';
import 'package:my_favorite_apps/repo/auth.dart';
import 'package:my_favorite_apps/screens/groups/components/TitleText.dart';
import 'package:url_launcher/url_launcher.dart';

class LoginScreen extends StatefulWidget {
  final IAuth auth;

  LoginScreen({this.auth});

  @override
  _LoginScreenState createState() => _LoginScreenState();

  static Future<void> navigate(BuildContext context, IAuth auth) async {
    await Navigator.of(context, rootNavigator: true).push(MaterialPageRoute(
        builder: (context) => LoginScreen(auth: auth), fullscreenDialog: true));
  }
}

class _LoginScreenState extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Login"),
      ),
      body: Stack(
        fit: StackFit.expand,
        children: [
          Padding(
              padding: EdgeInsets.only(top: 0.0),
              child: Container(
                color: Colors.transparent,
              )),
          SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.only(top: 50, left: 12, right: 12),
              child: Column(
                children: [
                  Image.asset("assets/images/splash.png",
                      width: 160, fit: BoxFit.cover),
                  SizedBox(
                    height: 16,
                  ),
                  Text(
                      "For a better experience, App requires you to Login using Your Google Email Address to remember your favorite apps. The information that App requests will be retained on your device and is not collected by me in any way.",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 16,
                          fontWeight: FontWeight.normal)),
                  SizedBox(
                    height: 16,
                  ),
                  Column(
                    children: [
                      TitleText(
                        title: "For more information, please check on ",
                      ),
                      FlatButton(
                          onPressed: () {
                            _launchURL() async {
                              const url =
                                  'https://oliversonnguyen.github.io/Apps-Management/';
                              if (await canLaunch(url)) {
                                await launch(url);
                              } else {
                                throw 'Could not launch $url';
                              }
                            }

                            _launchURL();
                          },
                          child: Text(
                            "Privacy Policy",
                            style: TextStyle(color: Colors.blue),
                          )),
                      Container(
                        height: 48,
                        width: 250,
                        decoration: BoxDecoration(
                            color: Colors.blue,
                            borderRadius: BorderRadius.circular(8)),
                        child: FlatButton(
                          onPressed: () {
                            widget.auth.signInWithGoogle();
                          },
                          child: Text(
                            'Login',
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 18,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
