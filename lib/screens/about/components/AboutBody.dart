import 'package:device_apps/device_apps.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:my_favorite_apps/screens/groups/components/TitleText.dart';
import 'package:url_launcher/url_launcher.dart';

class AboutBody extends StatelessWidget {
  Future<Application> loadVersion() async {
    Application app = await DeviceApps.getApp("ts.lap.favoriteapp");
    return app;
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      padding: const EdgeInsets.all(12),
      child: FutureBuilder<Application>(
          future: loadVersion(),
          builder: (context, snapshot) {
            return Column(
              //mainAxisAlignment: MainAxisAlignment.center,

              children: [
                Image.asset(
                  "assets/images/splash.png",
                  width: 120,
                  fit: BoxFit.cover,
                ),
                SizedBox(
                  height: 8,
                ),
                TitleText(
                  title: snapshot.data.appName,
                ),
                SizedBox(
                  height: 8,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    TitleText(
                      title: "Version: ",
                    ),
                    TitleText(
                      title: snapshot.data.versionCode.toString() +
                          " - " +
                          snapshot.data.versionName,
                    )
                  ],
                ),
                SizedBox(
                  height: 8,
                ),
                TitleText(
                  title: "To All users",
                ),
                SizedBox(
                  height: 8,
                ),
                TitleText(
                  title:
                      "The app is a useful tool to help everyone manage a personal app or a small favourite app.",
                ),
                SizedBox(
                  height: 8,
                ),
                TitleText(
                    title:
                        "Enjoy using App Management and let us know your feedback to improve the performance of the app."),
                SizedBox(
                  height: 8,
                ),
                Row(
                  children: [
                    TitleText(
                      title: "Email: ",
                    ),
                    FlatButton.icon(
                        onPressed: () {
                          var title = "Feedback_for_App_management";
                          final Uri _emailLaunchUri = Uri(
                              scheme: 'mailto',
                              path: 'tslabdeveloper@gmail.com',
                              queryParameters: {
                                'subject': title,
                              });

                          launch(_emailLaunchUri.toString());
                        },
                        icon: Icon(MdiIcons.emailOutline),
                        label: Text(
                          "tslabdeveloper@gmail.com",
                          style: TextStyle(color: Colors.blue),
                        )),
                  ],
                ),
              ],
            );
          }),
    );
  }
}
