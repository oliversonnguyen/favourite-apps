import 'package:flutter/material.dart';
import 'package:my_favorite_apps/screens/about/components/AboutBody.dart';

class AboutScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("About"),
      ),
      body: AboutBody(),
    );
  }
}
