import 'package:my_favorite_apps/data/app_item.dart';

class ScreenFavoriteModel {
  final List<AppItem> appItems;

  ScreenFavoriteModel({this.appItems});

}