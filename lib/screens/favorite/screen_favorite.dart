import 'package:flutter/material.dart';
import 'package:my_favorite_apps/config/IPlatform.dart';
import 'package:my_favorite_apps/data/app_item.dart';
import 'package:my_favorite_apps/database/database.dart';
import 'package:my_favorite_apps/global_data/app_data.dart';
import 'package:my_favorite_apps/repo/auth.dart';
import 'package:my_favorite_apps/screens/favorite/screen_favorite_bloc.dart';
import 'package:my_favorite_apps/screens/favorite/screen_favorite_model.dart';
import 'package:my_favorite_apps/screens/screens.dart';
import 'package:provider/provider.dart';

class FavoriteScreen extends StatelessWidget {
  final ScreenFavoriteBloc bloc;
  final Database database;

  const FavoriteScreen({this.bloc, this.database});

  static Widget create(BuildContext context) {
    final database = Provider.of<Database>(context);
    final auth = Provider.of<IAuth>(context);
    final appData = Provider.of<AppData>(context);
    final IPlatform platform = context.watch();
    return Provider<ScreenFavoriteBloc>(
      create: (_) => ScreenFavoriteBloc(database: database, auth: auth, appData: appData, platform: platform),
      dispose: (context, bloc) => bloc.dispose(),
      builder: (context, _) {
        return FavoriteScreen(
          bloc: context.watch(),
          database: database,
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    print(("favorite screen database:$database"));
    database.redFavorite();
    return StreamBuilder<List<AppItem>>(
      stream: bloc.favoriteStream(),
      initialData: [
        AppItem(name: "test1", desc: "test2", packageName: "test3")
      ],
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          final items = snapshot.data;

          return ListView.separated(
              padding: EdgeInsets.symmetric(vertical: 24.0, horizontal: 16.0),
              separatorBuilder: (context, index) {
                return SizedBox(height: 8.0);
              },
              itemCount: items.length,
              itemBuilder: (BuildContext context, int index) {
                final AppItem item = items[index];
                return AllAppItem(item: item, platform: bloc.platform,);
              });

        } else {
          return SizedBox.shrink();
        }
      },
    );
  }
}
