import 'dart:async';

import 'package:my_favorite_apps/config/IPlatform.dart';
import 'package:my_favorite_apps/data/app_item.dart';
import 'package:my_favorite_apps/database/database.dart';
import 'package:my_favorite_apps/global_data/app_data.dart';
import 'package:my_favorite_apps/repo/auth.dart';
import 'package:my_favorite_apps/screens/favorite/screen_favorite_model.dart';
import 'package:rxdart/rxdart.dart';

class ScreenFavoriteBloc {
  final IAuth auth;
  final Database database;
  final AppData appData;
  final IPlatform platform;

  ScreenFavoriteBloc({this.auth, this.database, this.appData, this.platform});

  final StreamController<ScreenFavoriteModel> streamController =
      StreamController<ScreenFavoriteModel>();

  Stream<ScreenFavoriteModel> get stream => streamController.stream;

  void dispose() {
    streamController.close();
  }

  Stream<List<AppItem>> favoriteStream() {
    return Rx.combineLatest2(
        database.favoriteStream(), appData.getAllAppsStream(),
        (List<AppItem> favList, List<AppItem> installedList) {
      List<AppItem> result = new List();
      favList.forEach((favItem) {
        var compairItem = installedList.firstWhere((installedItem) {
          return installedItem.packageName == favItem.packageName;
        }, orElse: () => null);

        if (compairItem != null) {
          result.add(compairItem);
        } else {
          result.add(favItem);
        }
      });
      return result;
    });

//    return database.favoriteStream();
  }
}
