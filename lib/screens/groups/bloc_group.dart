import 'dart:async';

import 'package:flutter/material.dart';
import 'package:my_favorite_apps/app_model/tags/Tag.dart';
import 'package:my_favorite_apps/data/app_item.dart';
import 'package:my_favorite_apps/database/database.dart';
import 'package:my_favorite_apps/repo/auth.dart';
import 'package:my_favorite_apps/screens/groups/GroupModel.dart';

class BlocGroup {
  final Database database;
  final IAuth auth;
  final AppItem appItem;

  String uid;
  BlocGroup({this.database, this.auth, this.appItem});

  Stream<List<Tag>> userTagStream() {
    return database.userTags(uid);
  }

  Stream<List<Tag>> appTags() {
    return database.getAppTags(appItem, uid);
  }



  final StreamController<GroupModel> streamController =
      StreamController<GroupModel>();

  Stream get stream => streamController.stream;

  Future<void> dispose() {
    streamController.close();
  }

  Future<void> init() async {
    await auth.currentUser().then((value) {
      uid = value.uid;
      print(">>>uid $uid");

      streamController.sink.add(GroupModel( item : appItem));
    });
  }

  GroupModel dummy() {
    return GroupModel(
      item: AppItem(
          name: "Netflix",
          desc: "Start Watching",
          icon: Icon(Icons.local_activity),
          isDownloaded: false),
    );
  }

  Future addTag(Tag tag) async {
    await database.addTag(tag);
  }

  Future<void> addAppTag(Tag tag, AppItem app) async{
    await database.addAppTag(tag, app);
  }

  Future<void> addTagToApp(Tag tag, AppItem app) async{
    await database.addTagToApp(app, tag);

  }

  Future<void> removeTagApp(Tag tag, AppItem app) async{
    await database.removeAppTag(app, tag);

  }
}
