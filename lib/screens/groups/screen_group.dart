import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:my_favorite_apps/data/app_item.dart';
import 'package:my_favorite_apps/database/database.dart';
import 'package:my_favorite_apps/repo/auth.dart';
import 'package:my_favorite_apps/screens/groups/GroupModel.dart';
import 'package:my_favorite_apps/screens/groups/bloc_group.dart';
import 'package:my_favorite_apps/screens/groups/components/GroupBodyScreen.dart';
import 'package:my_favorite_apps/widgets/loading.dart';
import 'package:my_favorite_apps/widgets_test/widget_test_yoyo.dart';
import 'package:provider/provider.dart';

class ScreenGroup extends StatefulWidget {
  final AppItem item;
  final BlocGroup bloc;

  const ScreenGroup({Key key, this.item, this.bloc}) : super(key: key);

  @override
  _ScreenGroup createState() => _ScreenGroup(item: item, bloc: bloc);

  static Future<void> navigate(BuildContext context, AppItem appItem) async {
    await Navigator.of(context, rootNavigator: true).push(MaterialPageRoute(
        builder: (context) => ScreenGroup.create(context, appItem),
        fullscreenDialog: true));
  }

  static Widget create(BuildContext context, AppItem item) {
    final Database database = context.watch();
    final IAuth auth = context.watch();
    return Provider<BlocGroup>(
      create: (_) => BlocGroup(database: database, auth: auth, appItem: item),
      dispose: (context, bloc) => bloc.dispose(),
      builder: (context, _) {
        return ScreenGroup(
          item: item,
          bloc: context.watch(),
        );
      },
    );
  }
}

class _ScreenGroup extends State<ScreenGroup> {
  final AppItem item;
  final BlocGroup bloc;

  _ScreenGroup({this.item, this.bloc});

  @override
  void initState() {
    super.initState();
    print(">>>group item:  ${item.toString()} - bloc: $bloc");
    bloc.init();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<GroupModel>(
      stream: bloc.stream,
      //initialData: bloc.dummy(),
      builder: (context, snapshot) {
        print(">>>group ${snapshot.hasData}");
        if (snapshot.hasData) {
          return GroupMain(data: snapshot.data, bloc: bloc);
        } else {
          //return GroupMain(data: snapshot.data);
          return Loading();
        }

      },
    );
  }
}

class GroupMain extends StatelessWidget {
  final GroupModel data;
  final BlocGroup bloc;

  const GroupMain({this.data, this.bloc}) ;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(MdiIcons.arrowLeft),
          onPressed: () {Navigator.pop(context);},
        ),
        title: Text(data.item.name),
      ),

      body: GroupBodyScreen(data: data,bloc: bloc,),
    );
  }
}

