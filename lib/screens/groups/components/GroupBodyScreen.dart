import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:my_favorite_apps/app_model/tags/Tag.dart';
import 'package:my_favorite_apps/screens/groups/GroupModel.dart';
import 'package:my_favorite_apps/screens/groups/bloc_group.dart';
import 'package:my_favorite_apps/widgets/loading.dart';
import 'package:my_favorite_apps/widgets_test/widget_test_yoyo.dart';

import 'TitleText.dart';

class GroupBodyScreen extends StatelessWidget {
  final GroupModel data;
  final BlocGroup bloc;

  const GroupBodyScreen({Key key, this.data, this.bloc}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.all(8.0),
              child: TitleText(title: "Group this app"),
            ),

            //
            // Loading(),
            AppTags(
              bloc: bloc,
              data: data,
            ),
            Padding(
              padding: EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  TitleText(title: "Your own groups"),
                  AddTagDialog(
                    bloc: bloc,
                  )
                ],
              ),
            ),

            UserOwnTagWidget(
              bloc: bloc,
              data: data,
            ),
          ],
        ),
      ),
    );
  }
}

class UserOwnTagWidget extends StatelessWidget {
  final BlocGroup bloc;
  final GroupModel data;

  const UserOwnTagWidget({Key key, this.bloc, this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<List<Tag>>(
        stream: bloc.userTagStream(),
        builder: (context, snapshot) {
          print(">>>tag UserOwnTagWidget ${snapshot.hasData}");
          if (snapshot.hasData) {
            final List<Tag> items = snapshot.data;
            print(">>>tag UserOwnTagWidget size ${items.length}");

            var returnPage = SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: List.generate(items.length, (index) {
                  final item = items[index];
                  return Row(
                    children: [
                      FlatButton.icon(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(12),
                            side: BorderSide(color: Colors.green),
                          ),
                          onPressed: () {
                            print(
                                ">>>Add item ${data.item.name} into group ${item.name}");
                            bloc.addAppTag(item, data.item);
                            bloc.addTagToApp(item, data.item);
                          },
                          icon: Icon(Icons.add),
                          label: TitleText(
                            title: item.name,
                          )),
                      SizedBox(
                        width: 8,
                      )
                    ],
                  );
                }),
              ),
            );

            return returnPage;
          } else {
            return TitleText(
              title: "No Group",
            );
          }
        });
  }
}

class AddTagDialog extends StatefulWidget {
  final BlocGroup bloc;

  const AddTagDialog({Key key, this.bloc}) : super(key: key);

  @override
  _AddTagDialogState createState() => _AddTagDialogState();
}

class _AddTagDialogState extends State<AddTagDialog> {
  final myController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return FlatButton.icon(
      onPressed: () async {
        var dialog1 = AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(24),
          ),
          elevation: 0,
          backgroundColor: Colors.black38,
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              TextField(
                controller: myController,
                style: TextStyle(color: Colors.white60),
              ),
              FlatButton(
                  onPressed: () {
                    Navigator.pop(context, myController.text);
                  },
                  child: TitleText(title: "OK")),
              FlatButton(
                  onPressed: () {
                    Navigator.pop(context, "");
                  },
                  child: TitleText(title: "Cancel")),
            ],
          ),
        );

        String tagName = await showDialog(context: context, child: dialog1);

        if (tagName.isNotEmpty) {
          widget.bloc.addTag(Tag(name: tagName));
        }
      },
      label: Text("Add tag"),
      icon: Icon(Icons.add),
    );
  }
}

class AppTags extends StatelessWidget {
  final BlocGroup bloc;
  final GroupModel data;

  const AppTags({Key key, this.bloc, this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<List<Tag>>(
        stream: bloc.appTags(),
        builder: (context, snapshot) {
          print(">>>AppTags ${snapshot.hasData}");
          if (snapshot.hasData) {
            final List<Tag> items = snapshot.data;
            print(">>>AppTags ${items.length}");

            var returnPage = SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: List.generate(items.length, (index) {
                  final item = items[index];
                  return Row(
                    children: [
                      FlatButton.icon(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(12),
                            side: BorderSide(color: Colors.blue),
                          ),
                          onPressed: () {
                            print(
                                ">>>removed item ${data.item.name} into group ${item.name}");
                            bloc.removeTagApp(item, data.item);
                          },
                          icon: Icon(Icons.remove),
                          label: TitleText(
                            title: item.name,
                          )),
                      SizedBox(
                        width: 8,
                      ),
                    ],
                  );
                }),
              ),
            );

            return returnPage;
          } else {
            return WidgetTestYoyo();
          }
        });
  }
}
