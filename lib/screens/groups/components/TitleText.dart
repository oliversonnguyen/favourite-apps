import 'package:flutter/material.dart';
import 'package:my_favorite_apps/config/palette.dart';

class TitleText extends StatelessWidget {
  final String title;

  const TitleText({Key key, this.title}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Text(title, style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold, color: Palette.titleColor), );
  }
}
