import 'package:flutter/material.dart';
import 'package:my_favorite_apps/database/database.dart';
import 'package:my_favorite_apps/repo/auth.dart';
import 'package:my_favorite_apps/screens/discover/components/BlocDiscover.dart';
import 'package:my_favorite_apps/screens/discover/components/DiscoverBody.dart';
import 'package:provider/provider.dart';

class DiscoverScreen extends StatefulWidget {
  final BlocDiscover bloc;

  DiscoverScreen({this.bloc});

  @override
  _DiscoverScreenState createState() => _DiscoverScreenState();

  static Widget create(BuildContext context) {
    final IAuth auth = context.watch();
    final Database database = context.watch();

    return Provider<BlocDiscover>(
      create: (_) => BlocDiscover(auth: auth, database: database),
      builder: (context, _) {
        return DiscoverScreen(
          bloc: context.watch(),
        );
      },
    );
  }
}

class _DiscoverScreenState extends State<DiscoverScreen> {
  @override
  Widget build(BuildContext context) {
    return DiscoverBody(bloc: widget.bloc,);
  }
}
