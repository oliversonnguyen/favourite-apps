import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:my_favorite_apps/data/AppShare.dart';

class ShareAppText extends StatelessWidget {
  final AppShare item;

  const ShareAppText({Key key, this.item}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    TextStyle defaultStyle = TextStyle(color: Colors.white, fontSize: 20.0);
    TextStyle linkStyle = TextStyle(color: Colors.blue);
    return RichText(
      text: TextSpan(
        style: defaultStyle,
        children: <TextSpan>[
          TextSpan(text: 'Shared '),
          TextSpan(
              text: item.appName,
              style: linkStyle,
              recognizer: TapGestureRecognizer()
                ..onTap = () {
                  print('Terms of Service"');
                }),
          TextSpan(text: ' " -> ${item.shareDesc}" '),

        ],
      ),
    );
  }
}
