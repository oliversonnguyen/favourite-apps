import 'package:my_favorite_apps/data/AppShare.dart';
import 'package:my_favorite_apps/database/database.dart';
import 'package:my_favorite_apps/repo/auth.dart';

class BlocDiscover {
  final IAuth auth;
  final Database database;

  BlocDiscover({this.auth, this.database});

  Stream<List<AppShare>> getAppsShare() {
    return database.getAppsShare();
  }



}