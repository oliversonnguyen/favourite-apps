import 'package:flutter/material.dart';
import 'package:my_favorite_apps/data/AppShare.dart';
import 'package:my_favorite_apps/screens/discover/components/AppShareRowWidget.dart';
import 'package:my_favorite_apps/screens/discover/components/BlocDiscover.dart';
import 'package:my_favorite_apps/screens/review/components/ReviewSectionWidget.dart';

class DiscoverBody extends StatefulWidget {
  final BlocDiscover bloc;

  const DiscoverBody({Key key, this.bloc}) : super(key: key);


  @override
  _DiscoverBodyState createState() => _DiscoverBodyState();
}

class _DiscoverBodyState extends State<DiscoverBody> {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<List<AppShare>>(
      stream: widget.bloc.getAppsShare(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          final List<AppShare> items = snapshot.data;



          var returnPage = SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: List.generate(items.length, (index) {
                final item = items[index];
                return AppShareRowWidget(item: item);
              }),
            ),
          );

          return returnPage;
        } else {
          return SizedBox.shrink();
        }
      },
    );
  }
}
