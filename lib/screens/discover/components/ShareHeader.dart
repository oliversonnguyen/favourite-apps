import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:jiffy/jiffy.dart';
import 'package:my_favorite_apps/global_data/ShareHeaderData.dart';
import 'package:my_favorite_apps/screens/review/components/ProfileAvatar.dart';

class ShareHeader extends StatelessWidget {
  final ShareHeaderData data;

  const ShareHeader({Key key, this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Timestamp createAt = data.createAt;
    var diff = DateTime.now().difference(createAt.toDate()).inDays;
    String display;
    if (diff >= 7) {
      var jiffy1 = Jiffy.unix(createAt.millisecondsSinceEpoch)
          .format("MMM do yyyy, h:mm:ss a");
      display = jiffy1;
    } else {
      var jiffy1 = Jiffy.unix(createAt.millisecondsSinceEpoch);
      display = jiffy1.fromNow();
    }

    return Row(
      children: <Widget>[
        ProfileAvatar(
          imageUrl: data.authorAvatar,
        ),
        const SizedBox(
          width: 8.0,
        ),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                data.author,
                style: const TextStyle(
                    fontWeight: FontWeight.w600, color: Colors.white),
              ),
              Row(
                children: <Widget>[
                  Text(
                    '$display - ',
                    style: TextStyle(color: Colors.white, fontSize: 12.0),
                  ),
                  data.icon,
                ],
              )
            ],
          ),
        ),
      ],
    );
  }
}
