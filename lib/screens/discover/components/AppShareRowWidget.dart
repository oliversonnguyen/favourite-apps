import 'package:flutter/material.dart';
import 'package:my_favorite_apps/data/AppShare.dart';
import 'package:my_favorite_apps/data/app_item.dart';
import 'package:my_favorite_apps/global_data/ShareHeaderData.dart';
import 'package:my_favorite_apps/screens/groups/components/TitleText.dart';
import 'package:my_favorite_apps/widgets/AppWidgetComponent/AppWidgetComponent.dart';

import 'ShareHeader.dart';

class AppShareRowWidget extends StatelessWidget {
  final AppShare item;

  const AppShareRowWidget({Key key, this.item}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var returnPage2 = Container(
      margin: const EdgeInsets.symmetric(vertical: 5.0, horizontal: 4),
      padding: const EdgeInsets.symmetric(
        vertical: 8.0,
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        color: Colors.black26,
      ),
      child: Column(children: <Widget>[
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 12.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              ShareHeader(
                data: ShareHeaderData(
                  uid: item.uid,
                  author: item.author,
                  authorAvatar: item.authorAvatar,
                  createAt: item.createAt,
                ),
              ),
              const SizedBox(
                width: 4.0,
              ),
              Row(
                children: [
                  TitleText(
                    title: "Shared ",
                  ),
                  AppWidgetComponent.create(context,
                      AppItem(name: item.appName, packageName: item.package)),
                ],
              ),
              FlatButton(
                color: Colors.white60,
                shape: BeveledRectangleBorder(
                  borderRadius: BorderRadius.circular(0),
                  side: BorderSide(color: Colors.grey, width: .2),
                ),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: TitleText(
                    title: item.shareDesc,
                  ),
                ),
              )
            ],
          ),
        ),
      ]),
    );
    return returnPage2;
  }
}
