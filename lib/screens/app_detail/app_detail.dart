import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:my_favorite_apps/config/IPlatform.dart';
import 'package:my_favorite_apps/data/app_item.dart';
import 'package:my_favorite_apps/database/database.dart';
import 'package:my_favorite_apps/repo/auth.dart';
import 'package:my_favorite_apps/screens/app_detail/app_detail_bloc.dart';
import 'package:my_favorite_apps/screens/app_detail/app_detail_model.dart';
import 'package:my_favorite_apps/screens/app_detail/components/FavoriteWidget.dart';
import 'package:my_favorite_apps/screens/app_detail/components/detail_body.dart';
import 'package:provider/provider.dart';

class AppDetail extends StatefulWidget {
  final AppItem appItem;
  final AppDetailBloc bloc;

  AppDetail({this.bloc, this.appItem});

  static Future<void> show(BuildContext context, AppItem appItem) async {
    await Navigator.of(context, rootNavigator: true).push(MaterialPageRoute(
        builder: (context) => AppDetail.create(context, appItem),
        fullscreenDialog: true));
  }

  static Widget create(BuildContext context, AppItem appItem) {
    final Database database = context.watch();
    final IAuth auth = context.watch();
    final IPlatform platform = context.watch();

    print("database : $database");

    return Provider<AppDetailBloc>(
      create: (_) => AppDetailBloc(
          database: database,
          auth: auth,
          appItem: appItem,
          uid: auth.uid(),
          platform: platform),
      dispose: (context, bloc) => bloc.dispose(),
      builder: (context, _) {
        return AppDetail(
          bloc: context.watch(),
          appItem: appItem,
        );
      },
    );
  }

  @override
  _AppDetailState createState() =>
      _AppDetailState(appItem: appItem, bloc: bloc);
}

class _AppDetailState extends State<AppDetail> {
  final AppItem appItem;
  final AppDetailBloc bloc;

  _AppDetailState({this.bloc, this.appItem});

  @override
  void initState() {
    print(">>>detail init");
    super.initState();
    bloc.init();
    print(">>>detail init ended");
  }

  @override
  Widget build(BuildContext context) {
    print(">>>detail build");
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(MdiIcons.arrowLeft),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        actions: [
          FavoriteWidget(
            bloc: bloc,
            appItem: appItem,
            auth: bloc.auth,
          ),
        ],
        centerTitle: true,
        title: Text(appItem.name),
      ),
      body: StreamBuilder<AppDetailModel>(
        stream: bloc.modelStream,
        initialData: AppDetailModel(appItem: appItem),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            final item = snapshot.data;
            return SingleChildScrollView(
              child: Column(
                children: [
                  DetailBody(
                    item: item.appItem,
                    bloc: bloc,
                  ),
                ],
              ),
            );
          } else {
            return SizedBox(
              child: Text("waiting load..."),
            );
          }
        },
      ),
//      floatingActionButton: FlatButton.icon(
//          onPressed: () {
//            WriteReviewScreen.navigate(context, appItem);
//          },
//          icon: Icon(MdiIcons.comment),label: Text("Comment"),
//          ),
    );
  }
}
