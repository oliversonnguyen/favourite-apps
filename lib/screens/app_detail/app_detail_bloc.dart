import 'dart:async';

import 'package:my_favorite_apps/config/IPlatform.dart';
import 'package:my_favorite_apps/data/AppReview.dart';
import 'package:my_favorite_apps/data/app_item.dart';
import 'package:my_favorite_apps/database/database.dart';
import 'package:my_favorite_apps/repo/auth.dart';
import 'package:my_favorite_apps/screens/app_detail/app_detail_model.dart';

class AppDetailBloc {
  final Database database;

  final IAuth auth;
  final AppItem appItem;
  final IPlatform platform;

  String uid;

  AppDetailBloc({this.database, this.auth, this.appItem, this.uid, this.platform});

  final StreamController<AppDetailModel> modelStreamController =
      StreamController<AppDetailModel>();

  Stream<AppDetailModel> get modelStream => modelStreamController.stream;

  void dispose() {
    modelStreamController.close();
  }

  Future<void> init() async {
    var user = await auth.currentUser();
    uid = user.uid;
    print(">>>uid $uid");
    auth.currentUser().then((value) {
      uid = value.uid;
      print(">>>uid $uid");
    });
  }



  Future<void> favorite(AppItem appItem) async {
    database.favoriteApp(appItem);
  }

  Future<void> removeFavorite(AppItem appItem) async {
    database.removeFavorite(appItem);
  }

  Future<void> addGroup(AppItem appItem) async {}

  Future<void> comment(AppItem appItem, String commend)  {
    database.addCommentToApp(appItem, commend);

  }

  Stream<bool> isFavorite() {
    return database.isAppFavorite(appItem, uid);
  }

  Stream<List<AppReview>> getAppReviews(AppItem item) {
    return database.getAppReviews(item);

  }

  Future<void> shareApp(AppItem item, String shareDesc) {
    database.shareApp(item, shareDesc);
  }

  Future<void> openApp(AppItem item) {
    return platform.openApp(item);
  }
}
