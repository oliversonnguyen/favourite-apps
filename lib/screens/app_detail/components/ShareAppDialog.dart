import 'package:flutter/material.dart';
import 'package:my_favorite_apps/data/app_item.dart';
import 'package:my_favorite_apps/screens/app_detail/app_detail_bloc.dart';
import 'package:my_favorite_apps/screens/groups/components/TitleText.dart';

class ShareApp extends StatefulWidget {
  final AppDetailBloc bloc;
  final AppItem item;

  const ShareApp({Key key, this.bloc, this.item}) : super(key: key);

  @override
  _ShareAppState createState() => _ShareAppState();
}

class _ShareAppState extends State<ShareApp> {
  final myController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return FlatButton.icon(
      onPressed: () async {
        String shareDesc = await showDialog(
            context: context,
            child: AlertDialog(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(8),
              ),
              content: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  TextField(
                    controller: myController,
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                  FlatButton(
                      onPressed: () {
                        Navigator.pop(context, myController.text);
                      },
                      child: TitleText(title: "OK")),
                  FlatButton(
                      onPressed: () {
                        Navigator.pop(context, "");
                      },
                      child: TitleText(title: "Cancel")),
                ],
              ),
            ));

        if (shareDesc.isNotEmpty) {
          widget.bloc.shareApp(widget.item, shareDesc);
          widget.bloc
              .comment(widget.item, "Shared this app with note: ${shareDesc}");
        }
      },
      label: Text("Share"),
      icon: Icon(Icons.share),
    );
  }
}
