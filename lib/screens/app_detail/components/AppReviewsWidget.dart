import 'package:flutter/widgets.dart';
import 'package:my_favorite_apps/data/AppReview.dart';
import 'package:my_favorite_apps/data/app_item.dart';
import 'package:my_favorite_apps/screens/app_detail/app_detail_bloc.dart';
import 'package:my_favorite_apps/screens/review/components/ReviewSectionWidget.dart';

class AppReviewsWidget extends StatefulWidget {
  final AppItem item;
  final AppDetailBloc bloc;

  const AppReviewsWidget({Key key, this.bloc, this.item}) : super(key: key);

  @override
  _AppReviewsWidgetState createState() => _AppReviewsWidgetState();
}

class _AppReviewsWidgetState extends State<AppReviewsWidget> {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<List<AppReview>>(
      stream: widget.bloc.getAppReviews(widget.item),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          final List<AppReview> items = snapshot.data;

          var returnPage = SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: List.generate(items.length, (index) {
                final item = items[index];
                return ReviewSectionWidget(item: item);
              }),
            ),
          );

          return returnPage;
        } else {
          return SizedBox.shrink();
        }
      },
    );
  }
}
