import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:my_favorite_apps/data/datas.dart';
import 'package:my_favorite_apps/repo/auth.dart';
import 'package:my_favorite_apps/screens/app_detail/app_detail_bloc.dart';
import 'package:my_favorite_apps/screens/login/LoginScreen.dart';

class FavoriteWidget extends StatelessWidget {
  final AppItem appItem;
  final AppDetailBloc bloc;
  final IAuth auth;

  const FavoriteWidget({this.bloc, this.appItem, this.auth});

  void favoriteApp(BuildContext context) async {
    var login = await auth.currentUser();
    if (login == null) {
      LoginScreen.navigate(context, auth);
    } else {
      bloc.favorite(appItem);
    }
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<bool>(
        stream: bloc.isFavorite(),
        builder: (context, snapshot) {
          print(">>>isfavorite widget ${snapshot.data}");
          if (snapshot.hasData) {
            final isFavorite = snapshot.data;
            return isFavorite
                ? IconButton(
                    icon: Icon(
                      MdiIcons.heart,
                      color: Colors.red,
                    ),
                    onPressed: () {
                      bloc.removeFavorite(appItem);
                    })
                : IconButton(
                    icon: Icon(MdiIcons.heartOutline),
                    onPressed: () {
                      favoriteApp(context);
                    });
          } else {
            return IconButton(
                icon: Icon(MdiIcons.heartOutline),
                onPressed: () {
                  favoriteApp(context);
                });
          }
        });
  }
}
