import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:my_favorite_apps/config/palette.dart';
import 'package:my_favorite_apps/data/app_item.dart';
import 'package:my_favorite_apps/screens/app_detail/app_detail_bloc.dart';
import 'package:my_favorite_apps/screens/app_detail/components/AppReviewsWidget.dart';
import 'package:my_favorite_apps/screens/app_detail/components/ShareAppDialog.dart';
import 'package:my_favorite_apps/screens/groups/components/TitleText.dart';
import 'package:my_favorite_apps/screens/groups/screen_group.dart';
import 'package:my_favorite_apps/screens/login/LoginScreen.dart';

class DetailBody extends StatelessWidget {
  final AppItem item;
  final AppDetailBloc bloc;

  DetailBody({this.item, this.bloc});

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          //top
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  width: 8.0,
                ),
                (item.myIcon != null)
                    ? Image.memory(
                        item.myIcon,
                        width: 60.0,
                        height: 60.0,
                      )
                    : SizedBox.shrink(),
                SizedBox(
                  width: 8.0,
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    //detail text
                    children: [
                      Text(
                        item.name,
                        style: TextStyle(
                            fontWeight: FontWeight.w600,
                            color: Palette.titleColor),
                      ),
                      Text(
                        item.packageName,
                        style: TextStyle(
                            fontWeight: FontWeight.w600,
                            color: Palette.subTitleColor),
                      ),
                      //group
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          FlatButton.icon(
                              onPressed: () {
                                void favoriteApp(BuildContext context) async {
                                  var login = await bloc.auth.currentUser();
                                  if (login == null) {
                                    LoginScreen.navigate(context, bloc.auth);
                                  } else {
                                    ScreenGroup.navigate(context, item);
                                  }
                                }

                                favoriteApp(context);
                                //ScreenGroup.navigate(context, item);
                              },
                              icon: Icon(MdiIcons.plus),
                              label: TitleText(title: "Add Tag")),
                        ],
                      ),
                    ],
                  ),
                ),
                FlatButton(
                  minWidth: 0,
                  //materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                  height: 28.0,
                  onPressed: () async {
                    bloc.openApp(item);
                  },
                  color: Colors.white12,

                  padding:
                      EdgeInsets.symmetric(vertical: 0.0, horizontal: 16.0),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12.0)),
                  child: Text(
                    item.isInstalled ? "OPEN" : "GET",
                    style: TextStyle(
                        color: Colors.blue,
                        fontWeight: FontWeight.bold,
                        fontSize: 14.0),
                  ),
                ),
              ],
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              ShareApp(
                bloc: bloc,
                item: item,
              ),
              FlatButton.icon(
                  onPressed: () async {
                    bloc.platform.openStore(item);
                  },
                  icon: Icon(MdiIcons.eye),
                  label: TitleText(title: "Play Store")),
            ],
          ),
          SizedBox(
            height: 16,
          ),
          AppReviewsWidget(
            bloc: bloc,
            item: item,
          ),
        ],
      ),
    );
  }
}
