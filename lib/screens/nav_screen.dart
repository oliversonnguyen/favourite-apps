import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:my_favorite_apps/config/IPlatform.dart';
import 'package:my_favorite_apps/database/database.dart';
import 'package:my_favorite_apps/global_data/app_data.dart';
import 'package:my_favorite_apps/repo/auth.dart';
import 'package:my_favorite_apps/screens/all_group/AllGroupScreen.dart';
import 'package:my_favorite_apps/screens/discover/DiscoverScreen.dart';
import 'package:my_favorite_apps/screens/favorite/screen_favorite.dart';
import 'package:my_favorite_apps/screens/login/LoginScreen.dart';
import 'package:my_favorite_apps/screens/screens.dart';
import 'package:my_favorite_apps/widgets/widgets.dart';
import 'package:provider/provider.dart';

class NavScreen extends StatefulWidget {
  final AppData app;
  final IPlatform platform;
  final IAuth auth;

  const NavScreen({Key key, this.app, this.platform, this.auth}) : super(key: key);

  @override
  _NavScreenState createState() => _NavScreenState(app: app, platform: platform, auth: auth);
}

class _NavScreenState extends State<NavScreen> {
  final AppData app;
  final IPlatform platform;
  final IAuth auth;

  _NavScreenState({this.app, this.platform, this.auth});

  GlobalKey<ScaffoldState> _key = new GlobalKey<ScaffoldState>();
  int _selectedIndex = 0;

  // final AppData app;

  // _NavScreenState(this.app);

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  void initState() {
    super.initState();

    //app.init();
  }

  Widget getScreen(int selected) {
    List<Widget> screens = [
      //AllAppsScreen(items: app.getAllAppItewms()),
      ScreenInstalledApps(app: app, platform: platform,),
      FavoriteScreen.create(context),

//      Container(
//        child: Text("Recent"),
//      ),
    ];
    var p1 = Container(
        child: Column(
      children: [
        // SearchBar(),
        CustomTabBar(),
        Expanded(child: TabBarView(children: screens)),
      ],
    ));

    var p2 = AllGroupScreen.create(context);
    var p3 = DiscoverScreen.create(context);

    launchScreen() async {
      var login = await auth.currentUser();
      if (login == null) {
        _selectedIndex = 0;
        _onItemTapped(_selectedIndex);
        LoginScreen.navigate(context, auth);
      }
    }

     switch  (_selectedIndex)  {
      case 0:
        {
          return p1;
        }

      case 1:
        {
          launchScreen();


          return p2;
        }

      default:
        {
          return p3;
        }
    }

  }

  @override
  Widget build(BuildContext context) {
    var home = Scaffold(
      body: getScreen(_selectedIndex),
      bottomNavigationBar: CustomNavigationBar(
          currentIndex: _selectedIndex, ontap: _onItemTapped),
    );

    var parentView = Provider<Database>(
      create: (_) => FireBaseStoreDatabase(),
      child: home,
    );

    var tabController = DefaultTabController(
      length: 2,
      child: parentView,
    );

    var main = Scaffold(
      key: _key,
      drawer: LeftDrawer(),
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(MdiIcons.menu),
          onPressed: () {
            _key.currentState.openDrawer();
            FocusScope.of(context).requestFocus(new FocusNode()); //remove focus

          },
        ),
        title: SearchBar(
          app: widget.app,
        ),
        centerTitle: false,
      ),
      body: tabController,
    );
    return main;
  }
}
