class Tag {
  final int rank;
  final String name;

  Tag({this.name, this.rank = 0});
}
