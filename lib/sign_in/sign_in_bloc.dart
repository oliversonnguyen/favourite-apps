import 'dart:async';

import 'package:my_favorite_apps/repo/auth.dart';

class SignInBloc {
  final IAuth auth;

  final StreamController<bool> _isLoadingController = StreamController<bool>();

  SignInBloc({this.auth});
  Stream<bool> get isLoadingStream => _isLoadingController.stream;

  void dispose() {
    _isLoadingController.close();
  }

  void setIsLoading(bool isLoading) => _isLoadingController.add(isLoading);

  Future<User> signInWithGoogle() async {
    try {
      setIsLoading(true);
      return await auth.signInWithGoogle();
    } catch (e) {
      rethrow;
    } finally {
      setIsLoading(false);
    }
  }

  Future<void> signOut() async {
    return await auth.signOut();
  }
}
