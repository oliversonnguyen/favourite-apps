// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'HiveAppItems.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class HiveAppItemsAdapter extends TypeAdapter<HiveAppItems> {
  @override
  final int typeId = 1;

  @override
  HiveAppItems read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return HiveAppItems(
      items: (fields[0] as List)?.cast<HiveAppItem>(),
    );
  }

  @override
  void write(BinaryWriter writer, HiveAppItems obj) {
    writer
      ..writeByte(1)
      ..writeByte(0)
      ..write(obj.items);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is HiveAppItemsAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
