import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
part 'HiveAppItem.g.dart';


@HiveType(typeId: 0)
class HiveAppItem extends HiveObject { 
  @HiveField(0)
  final String name;
  @HiveField(1)
  final String desc;
  @HiveField(2)
  final Icon icon;
  @HiveField(3)
  final bool isDownloaded;
  @HiveField(4)
  final Uint8List myIcon;
  @HiveField(5)
  final String packageName;
  @HiveField(6)
  final bool isInstalled;

  HiveAppItem({this.name, this.desc, this.icon, this.isDownloaded = false, this.myIcon, this.packageName,
  this.isInstalled = false});
  
}

class Test1 {
  
  
}


