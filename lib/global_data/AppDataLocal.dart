import 'dart:async';

import 'package:my_favorite_apps/data/app_item.dart';

abstract class AppDataLocal {
  Future<List<AppItem>> getAllApps();

  Stream<List<AppItem>> getAllAppsStream();

  Future<void> init();

  Future setData(List<AppItem> items);

  Future<void> clean();
}

class AppDataLocalImpl extends AppDataLocal {
  StreamController<List<AppItem>> streamController =
      new StreamController.broadcast();

  List<AppItem> items;

  @override
  Future<List<AppItem>> getAllApps() async {
    if (items == null) {
      return null;
    }
    return Future.value(items);
  }

  @override
  Stream<List<AppItem>> getAllAppsStream() {
    //streamController.sink.add(items);
    return streamController.stream;
  }

  @override
  Future<void> setData(List<AppItem> data) {
    //print(">>>setData $data");
    // this.items.addAll(data);
    items = data;
    streamController.sink.add(items);
  }

  @override
  Future<void> init() {}

  @override
  Future<void> clean() {
    items = null;
  }
}
