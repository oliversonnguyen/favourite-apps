import 'package:hive/hive.dart';
import 'package:my_favorite_apps/data/app_item.dart';
import 'package:my_favorite_apps/global_data/HiveAppItem.dart';
import 'package:my_favorite_apps/global_data/HiveAppItems.dart';

abstract class DiskCachedData {
  Future<List<AppItem>> getAllApps();

  Future setData(List<AppItem> items);

  Future<void> clean();
}

class DiskCachedDataImpl extends DiskCachedData {
  @override
  Future<List<AppItem>> getAllApps() async {
    var box = await Hive.openBox("fav_app_items");

    HiveAppItems items = box.get("data");
    if (items == null) {
      return null;
    }
    if (items.items == null || items.items.length == 0) {
      return null;
    }

    List<AppItem> list = new List();
    items.items.forEach((e) {
      var item = AppItem(
          name: e.name,
          desc: e.desc,
          icon: e.icon,
          isDownloaded: e.isDownloaded,
          myIcon: e.myIcon,
          packageName: e.packageName,
          isInstalled: e.isInstalled);
      list.add(item);
    });

    return Future.value(list);
  }

  @override
  Future setData(List<AppItem> items) async {
    var box = await Hive.openBox("fav_app_items");

    List<HiveAppItem> list = new List();

    items.forEach((e) {
      var item = HiveAppItem(
          name: e.name,
          desc: e.desc,
          icon: e.icon,
          isDownloaded: e.isDownloaded,
          myIcon: e.myIcon,
          packageName: e.packageName,
          isInstalled: e.isInstalled);

      list.add(item);
    });

    box.put("data", HiveAppItems(items: list));
  }

  @override
  Future<void> clean() async {
    var box = await Hive.openBox("fav_app_items");
    box.delete("data");
  }
}
