import 'dart:async';
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:my_favorite_apps/config/IPlatform.dart';
import 'package:my_favorite_apps/data/app_item.dart';
import 'package:device_apps/device_apps.dart';

import 'package:my_favorite_apps/data/datas.dart';
import 'package:my_favorite_apps/global_data/AppDataLocal.dart';
import 'package:my_favorite_apps/global_data/DiskCachedData.dart';
import 'package:my_favorite_apps/global_data/SourceData.dart';

abstract class AppData {
  Future<List<AppItem>> getAllApps();

  Future<List<AppItem>> getAllAppsWithCache();

  Stream<List<AppItem>> getAllAppsWithCacheStream();

  Future<List<AppItem>> getAllAppsWithSearch(String keyword);

  Stream<List<AppItem>> getAllAppsStream();

  Future<void> init();

  Future<void> cleanCached();

  Future<void> triggerSearch({String keyword});

  Future<AppItem> searchItem(String package);
}

class AllAppData implements AppData {
  final SourceData source;
  final AppDataLocal local;
  final DiskCachedData diskCachedData;

  AllAppData(this.source, this.local, this.diskCachedData);

//  AppDataLocal local = AppDataLocalImpl();
//  SourceData source = SourceDataImpl(platform);
//  DiskCachedData diskCachedData = DiskCachedDataImpl();
  String keyword = "";

  StreamController<List<AppItem>> streamController =
      new StreamController.broadcast();

  List<AppItem> dummyList = [
    AppItem(
        name: "ZOOM Cloud Meetings",
        desc: "Meet Happy",
        icon: Icon(Icons.gamepad),
        isDownloaded: false),
    AppItem(
        name: "YouTube: Watch, Listen, Stream",
        desc: "Videos, Music and Live Streams",
        icon: Icon(Icons.message),
        isDownloaded: false),
    AppItem(
        name: "Microsoft Teams",
        desc: "Hub for teamwork",
        icon: Icon(Icons.mic),
        isDownloaded: false),
    AppItem(
        name: "Netflix",
        desc: "Start Watching",
        icon: Icon(Icons.local_activity),
        isDownloaded: false),
    AppItem(
        name: "Google Chrome",
        desc: "Fast & Secure Web Browser",
        icon: Icon(Icons.handyman),
        isDownloaded: true),
  ];

  Future<List<AppItem>> fetchInstalledApps() async {
    List<AppItem> returnValue = new List();

    List<Application> apps = await DeviceApps.getInstalledApplications(
      includeAppIcons: true,
      includeSystemApps: false,
      onlyAppsWithLaunchIntent: true,
    );
    apps.forEach((element) {
      Uint8List myIcon = (element as ApplicationWithIcon).icon;
      debugPrint(element.toString());
      returnValue.add(AppItem(
          name: element.appName,
          myIcon: myIcon,
          desc: element.category.toString(),
          packageName: element.packageName,
          isInstalled: true));
    });
    return returnValue;
  }

  @override
  Future<List<AppItem>> getAllApps() async {
    if (Platform.isAndroid) {
      // return fetchInstalledApps();
      return dummyList;
    } else {
      return dummyList;
    }
  }

  @override
  Future<List<AppItem>> getAllAppsWithCache() async {
    final cacheData = await local.getAllApps();
    print(">>>getAllAppsWithCache cacheData NULL? ${cacheData == null}");
    if (cacheData != null) {
      return cacheData;
    }

    final cachedDisk = await diskCachedData.getAllApps();
    print(">>>getAllAppsWithCache cachedDisk ");
    if (cachedDisk != null) {
      local.setData(cachedDisk);
      return cachedDisk;
    }

    final fetchedData = await source.getAllApps();
    print(">>>getAllAppsWithCache fetchedData ");
    diskCachedData.setData(fetchedData);
    local.setData(fetchedData);
    return fetchedData;
  }

  @override
  Future<void> init() async {
//    var items = await getAllApps();
//    local.setData(items);

    var items = await getAllAppsWithSearch(keyword);
    streamController.sink.add(items);
  }

  @override
  Stream<List<AppItem>> getAllAppsStream() {
    return getAllAppsWithSearch("").asStream();
  }

  @override
  Future<void> cleanCached() async {
    local.clean();
    diskCachedData.clean();
    triggerSearch(keyword: keyword);
  }

  @override
  Future<List<AppItem>> getAllAppsWithSearch(String keyword) async {
    if (keyword == null || keyword.isEmpty) {
      return getAllAppsWithCache();
    }

    var items = await getAllAppsWithCache();
    var result = items
        .where((element) =>
            element.name.toLowerCase().contains(keyword.toLowerCase().trim()))
        .toList();
    return result;
  }

  @override
  Future<void> triggerSearch({String keyword}) async {
    print(">>>trigger search $keyword");
    this.keyword = keyword;

    var items = await getAllAppsWithSearch(keyword);
    streamController.sink.add(items);
//    getAllAppsWithCacheStream();
  }

  @override
  Stream<List<AppItem>> getAllAppsWithCacheStream() {
    return streamController.stream;

//    return getAllAppsWithSearch(keyword).asStream();
  }

  @override
  Future<AppItem> searchItem(String package) async {
    final cacheData = await local.getAllApps();
    if (cacheData != null) {
      return cacheData.where((element) => element.packageName == package).first;
    }

    final cachedDisk = await diskCachedData.getAllApps();
    if (cachedDisk != null) {
      local.setData(cachedDisk);
      return cachedDisk
          .where((element) => element.packageName == package)
          .first;
    }

    final fetchedData = await source.getAllApps();
    diskCachedData.setData(fetchedData);
    local.setData(fetchedData);
    return fetchedData.where((element) => element.packageName == package).first;
  }
}
