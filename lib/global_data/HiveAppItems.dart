import 'package:hive/hive.dart';
import 'package:my_favorite_apps/global_data/HiveAppItem.dart';
part 'HiveAppItems.g.dart';

@HiveType(typeId: 1)
class HiveAppItems extends HiveObject {
  @HiveField(0)
  final List<HiveAppItem> items;

  HiveAppItems({this.items});

}