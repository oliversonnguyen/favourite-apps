// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'HiveAppItem.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class HiveAppItemAdapter extends TypeAdapter<HiveAppItem> {
  @override
  final int typeId = 0;

  @override
  HiveAppItem read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return HiveAppItem(
      name: fields[0] as String,
      desc: fields[1] as String,
      icon: fields[2] as Icon,
      isDownloaded: fields[3] as bool,
      myIcon: fields[4] as Uint8List,
      packageName: fields[5] as String,
      isInstalled: fields[6] as bool,
    );
  }

  @override
  void write(BinaryWriter writer, HiveAppItem obj) {
    writer
      ..writeByte(7)
      ..writeByte(0)
      ..write(obj.name)
      ..writeByte(1)
      ..write(obj.desc)
      ..writeByte(2)
      ..write(obj.icon)
      ..writeByte(3)
      ..write(obj.isDownloaded)
      ..writeByte(4)
      ..write(obj.myIcon)
      ..writeByte(5)
      ..write(obj.packageName)
      ..writeByte(6)
      ..write(obj.isInstalled);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is HiveAppItemAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
