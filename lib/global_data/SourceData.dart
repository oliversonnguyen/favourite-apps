import 'dart:async';
import 'dart:typed_data';

import 'package:device_apps/device_apps.dart';
import 'package:flutter/material.dart';
import 'package:my_favorite_apps/config/IPlatform.dart';
import 'package:my_favorite_apps/data/app_item.dart';


abstract class SourceData {
  Future<List<AppItem>> getAllApps();
}

class SourceDataImpl extends SourceData {
  final IPlatform platform;

  SourceDataImpl(this.platform);
  @override
  Future<List<AppItem>> getAllApps() async {
    return fetchInstalledApps();
    //  return Future.delayed(
    //   Duration(seconds: 5),
    //   () => dummyList,
    // );
  }

  List<AppItem> dummyList = [
    AppItem(
        name: "ZOOM Cloud Meetings",
        desc: "Meet Happy",
        icon: Icon(Icons.gamepad),
        isDownloaded: false),
    AppItem(
        name: "YouTube: Watch, Listen, Stream",
        desc: "Videos, Music and Live Streams",
        icon: Icon(Icons.message),
        isDownloaded: false),
    AppItem(
        name: "Microsoft Teams",
        desc: "Hub for teamwork",
        icon: Icon(Icons.mic),
        isDownloaded: false),
    AppItem(
        name: "Netflix",
        desc: "Start Watching",
        icon: Icon(Icons.local_activity),
        isDownloaded: false),
    AppItem(
        name: "Google Chrome",
        desc: "Fast & Secure Web Browser",
        icon: Icon(Icons.handyman),
        isDownloaded: true),
  ];

  Future<List<AppItem>> fetchInstalledApps() async {
    List<AppItem> returnValue = new List();

    if (platform.platform() == AppPlatform.ANDROID) {
      List<Application> apps = await DeviceApps.getInstalledApplications(
        includeAppIcons: true,
        includeSystemApps: false,
        onlyAppsWithLaunchIntent: true,
      );
      apps.forEach((element) {
        Uint8List myIcon = (element as ApplicationWithIcon).icon;
        debugPrint(element.toString());
        returnValue.add(AppItem(
            name: element.appName,
            myIcon: myIcon,
            desc: element.category.toString(),
            packageName: element.packageName,
            isInstalled: true));
      });
      return returnValue
        ..sort((a, b) {
          return a.name.toLowerCase().compareTo(b.name.toLowerCase());
        });
    } else {




      returnValue.addAll(dummyList);
      return returnValue;
    }


  }
}
