import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ShareHeaderData {
  final String author;
  final String uid;
  final String authorAvatar;
  final Timestamp createAt;
  final Icon icon;

  ShareHeaderData({
    this.author,
    this.uid,
    this.authorAvatar,
    this.createAt,
    this.icon = const Icon(Icons.public, color: Colors.white, size: 12.0),
  });
}
