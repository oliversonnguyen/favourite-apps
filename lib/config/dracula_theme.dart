import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:my_favorite_apps/config/palette.dart';

var themeDracula = ThemeData(

  brightness: Brightness.dark,
  primarySwatch: Colors.orange,
  accentColor: Colors.green,
  visualDensity: VisualDensity.adaptivePlatformDensity,



  textTheme: GoogleFonts.robotoTextTheme().apply(
    displayColor: Palette.subTitleColor,
  ),
  appBarTheme: AppBarTheme(
    color: Colors.transparent,
    elevation: 0,
    brightness: Brightness.dark,
  )
);
