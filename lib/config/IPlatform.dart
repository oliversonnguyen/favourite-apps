import 'dart:io';
import 'package:device_apps/device_apps.dart';
import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:my_favorite_apps/data/app_item.dart';
import 'package:url_launcher/url_launcher.dart';

enum AppPlatform { ANDROID, IOS, WEB }

abstract class IPlatform {
  AppPlatform platform();

  Future<void> openApp(AppItem item);

  Future<void> openStore(AppItem item);
}

class PlatformImpl extends IPlatform {
  @override
  AppPlatform platform() {
    if (Platform.isAndroid) {
      return AppPlatform.ANDROID;
    }
    if (Platform.isIOS) {
      return AppPlatform.IOS;
    }
    if (kIsWeb) {
      return AppPlatform.WEB;
    }
    return AppPlatform.ANDROID;
  }

  @override
  Future<void> openApp(AppItem item) async {
    var package = item.packageName;
    print(">>>open app: $package on ${platform()}");
    switch (platform()) {
      case AppPlatform.ANDROID:
        {
          var isInstalled = await DeviceApps.isAppInstalled(item.packageName);
          if (isInstalled) {
            DeviceApps.openApp(item.packageName);
          } else {
            var url =
                "https://play.google.com/store/apps/details?id=${item.packageName}";
            print(">>>url: $url");
            await launch(url);
          }
        }
        break;
      default:
        {}
        break;
    }
  }

  @override
  Future<void> openStore(AppItem item) async {
    switch (platform()) {
      case AppPlatform.ANDROID:
        {
          var url =
              "https://play.google.com/store/apps/details?id=${item.packageName}";
          print(">>>url: $url");
          await launch(url);
        }
        break;

      default:
        {}
        break;
    }
  }
}
