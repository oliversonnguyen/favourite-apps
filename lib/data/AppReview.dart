import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:my_favorite_apps/data/app_item.dart';

class AppReview {
  final String author;
  final String authorAvatar;
  final String authorUid;
  final AppItem item;
  final String comment;
  final Timestamp createAt;

  AppReview({this.author, this.authorAvatar, this.authorUid, this.item, this.comment, this.createAt});


}