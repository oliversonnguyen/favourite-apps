import 'package:cloud_firestore/cloud_firestore.dart';

class AppShare {
  final String author;
  final String uid;
  final String authorAvatar;
  final String shareDesc;
  final Timestamp createAt;
  final String appName;
  final String package;
  final String appDesc;

  AppShare({this.author, this.uid, this.authorAvatar, this.shareDesc, this.createAt, this.appName, this.package, this.appDesc});




}

