import 'dart:typed_data';

import 'package:flutter/material.dart';

class AppItem { 
  final String name;
  final String desc;
  final Icon icon;
  final bool isDownloaded;
  final Uint8List myIcon;
  final String packageName;
  final bool isInstalled;

  const AppItem({this.name, this.desc, this.icon, this.isDownloaded = false, this.myIcon , this.packageName,
  this.isInstalled = false});
  
}
  