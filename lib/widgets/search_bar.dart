import 'package:flutter/material.dart';
import 'package:my_favorite_apps/config/palette.dart';
import 'package:my_favorite_apps/global_data/app_data.dart';

class SearchBar extends StatefulWidget {
  final AppData app;

  SearchBar({this.app});

  @override
  _SearchBarState createState() {
    return _SearchBarState();
  }
}

class _SearchBarState extends State<SearchBar> with WidgetsBindingObserver {
  bool isFocus = false;
  final TextEditingController textEditingController = TextEditingController();

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    super.dispose();
    print(">>>AAAA:dispose  mounted :$mounted");
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    print(">>>AAAA:didChangeAppLifecycleState  state :$state");
    if (state == AppLifecycleState.resumed) {
      setState(() {
        isFocus = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    textEditingController.addListener(() {
      widget.app.triggerSearch(keyword: textEditingController.text);
    });
    return Container(
      height: 36.0,
      //margin: EdgeInsets.only(top: 30),
      padding: EdgeInsets.symmetric(horizontal: 8, vertical: 0),
      decoration: BoxDecoration(
        color: Colors.white12,
        borderRadius: BorderRadius.circular(6),
      ),
      child: TextField(
        autofocus: isFocus,
        style: TextStyle(
            fontSize: 14.0,
            fontWeight: FontWeight.w600,
            color: Palette.titleColor),
        controller: textEditingController,
        decoration: InputDecoration(
          hintText: "Search ...",
          hintStyle: TextStyle(fontSize: 14.0, fontWeight: FontWeight.w600),
          icon: Icon(Icons.search_rounded),
          suffixIcon: IconButton(
            icon: Icon(Icons.clear),
            onPressed: () {
              textEditingController.clear();
            },
          ),
          border: InputBorder.none,
        ),
      ),
    );
  }
}
