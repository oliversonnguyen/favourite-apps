import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class CustomNavigationBar extends StatelessWidget {
  final int currentIndex;
  final Function ontap;

  const CustomNavigationBar({Key key, this.currentIndex, this.ontap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      items: [
        BottomNavigationBarItem(
          icon: Icon(Icons.home_outlined),
          activeIcon: Icon(Icons.home),
          label: "Home",
        ),
        BottomNavigationBarItem(
          icon: Icon(MdiIcons.tagOutline),
          activeIcon: Icon(MdiIcons.tag),
          label: 'Tag',
        ),
        BottomNavigationBarItem(
          icon: Icon(MdiIcons.networkOutline),
          activeIcon: Icon(MdiIcons.network),
          label: 'Discover',
        ),
      ],
      currentIndex: currentIndex,
      onTap: ontap,
    );
  }
}
