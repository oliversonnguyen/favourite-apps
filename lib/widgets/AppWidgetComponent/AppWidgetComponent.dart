import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:my_favorite_apps/config/IPlatform.dart';
import 'package:my_favorite_apps/data/app_item.dart';
import 'package:my_favorite_apps/global_data/app_data.dart';
import 'package:my_favorite_apps/screens/app_detail/app_detail.dart';
import 'package:my_favorite_apps/screens/groups/components/TitleText.dart';

import 'BlocAppWidgetComponent.dart';
import 'package:provider/provider.dart';

class AppWidgetComponent extends StatelessWidget {
  final BlocAppWidgetComponent bloc;
  final AppItem item;

  const AppWidgetComponent({Key key, this.bloc, this.item}) : super(key: key);

  static Widget create(BuildContext context, AppItem item) {
    final IPlatform platform = context.watch();
    final AppData appData = context.watch();
    return Provider<BlocAppWidgetComponent>(
      create: (_) =>
          BlocAppWidgetComponent(platform: platform, appData: appData),
      builder: (context, _) {
        return AppWidgetComponent(
          bloc: context.watch(),
          item: item,
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    var returnWidget = FutureBuilder<AppItem>(
        future: bloc.getApp(item),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            var data = snapshot.data;

            var flatButton = FlatButton(
              onPressed: () {
                FocusScope.of(context).unfocus();
                AppDetail.show(context, data);
              },
              color: Colors.transparent,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(8),
                side: BorderSide(
                  color: Colors.green,
                  width: 1,
                ),
              ),
              child: Row(
                children: [
                  (data.myIcon != null)
                      ? Image.memory(
                          data.myIcon,
                          width: 20.0,
                          height: 20.0,
                        )
                      : SizedBox.shrink(),
                  SizedBox(
                    width: 8,
                  ),
                  TitleText(
                    title: data.name,
                  )
                ],
              ),
            );

            return flatButton;
          } else {
            return TitleText(
              title: "AppWidgetComponent no data",
            );
          }
        });

    return returnWidget;
  }
}
