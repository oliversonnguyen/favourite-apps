import 'package:device_apps/device_apps.dart';
import 'package:my_favorite_apps/config/IPlatform.dart';
import 'package:my_favorite_apps/data/app_item.dart';
import 'package:my_favorite_apps/global_data/app_data.dart';

class BlocAppWidgetComponent {
  final IPlatform platform;
  final AppData appData;

  BlocAppWidgetComponent({this.platform, this.appData});

  Stream<AppItem> streamApp(AppItem item) {}

  Future<AppItem> getApp(AppItem item) async {
    if (platform.platform() == AppPlatform.ANDROID) {
      var isInstalled = await DeviceApps.isAppInstalled(item.packageName);
      if (isInstalled) {
        return appData.searchItem(item.packageName);
      } else {
        return Future.value(item);
      }
    }
  }

  Future<void> openApp(AppItem item) async {
    return platform.openApp(item);
  }
}
