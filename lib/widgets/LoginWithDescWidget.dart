import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:my_favorite_apps/repo/auth.dart';
import 'package:my_favorite_apps/screens/groups/components/TitleText.dart';

class LoginWithDescWidget extends StatelessWidget {
  final String desc;
  final IAuth auth;

  const LoginWithDescWidget({Key key, this.desc, this.auth}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        TitleText(
          title: desc,
        ),
        SizedBox(
          height: 16,
        ),
        TitleText(
          title: "You need to login first to use this feature",
        ),
        FlatButton.icon(
            onPressed: () {
              auth.signInWithGoogle();
            },
            icon: Icon(MdiIcons.login),
            label: TitleText(
              title: "Login",
            )),
      ],
    );
  }
}
