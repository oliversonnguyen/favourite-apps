
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:my_favorite_apps/global_data/app_data.dart';
import 'package:my_favorite_apps/repo/auth.dart';
import 'package:my_favorite_apps/repo/repos.dart';
import 'package:my_favorite_apps/screens/about/AboutScreen.dart';
import 'package:my_favorite_apps/screens/groups/components/TitleText.dart';
import 'package:my_favorite_apps/screens/login/LoginScreen.dart';
import 'package:my_favorite_apps/sign_in/sign_in_bloc.dart';

import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

class LeftDrawer extends StatelessWidget {
  // final IAuth auth;

  // const LeftDrawer({Key key, this.auth}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final IAuth auth = Provider.of(context);
    print("LeftDrawer auth: $auth with ${auth.hashCode}");
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: [
          CustomHeader(
              //auth: auth,
              ),
          DrawerListItem(
              // auth: auth,
              ),
        ],
      ),
    );
  }
}

class DrawerListItem extends StatelessWidget {

  
  @override
  Widget build(BuildContext context) {
    final IAuth auth = Provider.of<IAuth>(context);
    final AppData appData = context.watch();
    var logOutView;

    _launchURL() async {
      const url = 'https://oliversonnguyen.github.io/Apps-Management/';
      if (await canLaunch(url)) {
        await launch(url);
      } else {
        throw 'Could not launch $url';
      }
    }

    var streamBuilder = StreamBuilder<User>(
        stream: auth.onAuthStateChanged,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            logOutView = ListTile(
              title: TitleText(title: "Logout",),
              onTap: () {
                auth.signOut();
              },
            );
          } else {
            logOutView = SizedBox.shrink();
            // return Loading();
          }

          return Column(
            children: [
              ListTile(
                title: TitleText(title: "Clean Cached"),
                onTap:  () {
                  appData.cleanCached();
                },
              ),
              logOutView,
              ListTile(
                title: TitleText(title: "Privacy Policy",),
                onTap: () {
                  _launchURL();

                },
              ),
              ListTile(
                title: TitleText(title: "About",),
                onTap: () {
                  Navigator.push(context, MaterialPageRoute<void>(
                    builder: (_) => AboutScreen(),
                  ));

                },
              )
            ],
          );
        });

    return streamBuilder;
  }
}

class CustomHeader extends StatelessWidget {
  // final IAuth auth;

  // const CustomHeader({@required this.auth});

  @override
  Widget build(BuildContext context) {
    final IAuth auth = Provider.of<IAuth>(context);
    var streamBuilder = StreamBuilder<User>(
        stream: auth.onAuthStateChanged,
        builder: (context, snapshot) {
          var returnPage;
          if (snapshot.hasData) {
            returnPage = ProfilePage(
              user: snapshot.data,
            );
          } else {
            returnPage = LoginPage.create(context);
          }
          return returnPage;
        });
    return streamBuilder;
  }
}

class LoginPage extends StatelessWidget {
  final SignInBloc bloc;
  

  const LoginPage({@required this.bloc}) ;

  static Widget create(BuildContext context) {
    // final auth = Provider.of<IAuth>(context);
    final IAuth auth = context.watch();
    print("Login auth: $auth with ${auth.hashCode}");
    return Provider<SignInBloc>(
      create: (_) => SignInBloc(auth: auth),
      dispose: (context, bloc) => bloc.dispose(),
      // child : SizedBox.shrink(),
      
      // child: Consumer(builder:(_,__,____) {
      //   return SizedBox.shrink();

      // }),

      builder: (context, _) {
        // return SizedBox.shrink();
        return LoginPage(bloc: context.watch<SignInBloc>());

      },

      // child: Consumer(
        

      //   builder: (context, bloc, _)  {
      //      print("Login bloc: $bloc with ${bloc.hashCode}");
      //      return SizedBox.shrink();
      //     //return LoginPage(bloc: bloc,);
      //     },
      // ),
    );
  }

  // const LoginPage({
  //    this.auth,
  // });
  

  @override
  Widget build(BuildContext context) {
    // IAuth auth;

    // auth = Provider.of<IAuth>(context);

    var returnPage = DrawerHeader(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          FlatButton(
             // onPressed: () => _signInWithGoogle(),
              onPressed: () => LoginScreen.navigate(context, bloc.auth ),
              child: Icon(MdiIcons.accountCircleOutline)),
          SizedBox(
            height: 16.0,
          ),
          TitleText(title: "Click profile picture to log in",)

        ],
      ),
    );

    var streamBuilder = StreamBuilder<bool>(
        stream: bloc.isLoadingStream,
        initialData: false,
        builder: (context, snapshot) {
          return returnPage;
        });

    return streamBuilder;
  }

  Future<void> _signInWithGoogle() async {
    try {
      
      await bloc.signInWithGoogle();
    } catch (e) {
      print(e.toString());
    }
  }
}

class ProfilePage extends StatelessWidget {
  final User user;

  const ProfilePage({this.user});
  @override
  Widget build(BuildContext context) {
    return DrawerHeader(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          FlatButton(
              onPressed: () {
                print("click on avatar");
              },
              child: CircleAvatar(
                radius: 17,
                backgroundColor: Colors.grey[200],
                backgroundImage: CachedNetworkImageProvider(user.avatarUrl),
              )),
          SizedBox(
            height: 16.0,
          ),
          TitleText(title: "Hi, ${user.displayName}!",),

        ],
      ),
    );
  }
}
